Package        : linux
CVE ID         : CVE-2021-44879 CVE-2023-5178 CVE-2023-5197 CVE-2023-5717 CVE-2023-6121 CVE-2023-6531 CVE-2023-6817 CVE-2023-6931 CVE-2023-6932 CVE-2023-25775 CVE-2023-34324 CVE-2023-35827 CVE-2023-45863 CVE-2023-46813 CVE-2023-46862 CVE-2023-51780 CVE-2023-51781 CVE-2023-51782

Several vulnerabilities have been discovered in the Linux kernel that
may lead to a privilege escalation, denial of service or information
leaks.

CVE-2021-44879

    Wenqing Liu reported a NULL pointer dereference in the f2fs
    implementation. An attacker able to mount a specially crafted image
    can take advantage of this flaw for denial of service.

CVE-2023-5178

    Alon Zahavi reported a use-after-free flaw in the NVMe-oF/TCP
    subsystem in the queue initialization setup, which may result in
    denial of service or privilege escalation.

CVE-2023-5197

    Kevin Rich discovered a use-after-free flaw in the netfilter
    subsystem which may result in denial of service or privilege
    escalation for a user with the CAP_NET_ADMIN capability in any user
    or network namespace.

CVE-2023-5717

    Budimir Markovic reported a heap out-of-bounds write vulnerability
    in the Linux kernel's Performance Events system caused by improper
    handling of event groups, which may result in denial of service or
    privilege escalation. The default settings in Debian prevent
    exploitation unless more permissive settings have been applied in
    the kernel.perf_event_paranoid sysctl.

CVE-2023-6121

    Alon Zahavi reported an out-of-bounds read vulnerability in the
    NVMe-oF/TCP which may result in an information leak.

CVE-2023-6531

    Jann Horn discovered a use-after-free flaw due to a race condition
    when the unix garbage collector's deletion of a SKB races
    with unix_stream_read_generic() on the socket that the SKB is
    queued on.

CVE-2023-6817

    Xingyuan Mo discovered that a use-after-free in Netfilter's
    implementation of PIPAPO (PIle PAcket POlicies) may result in denial
    of service or potential local privilege escalation for a user with
    the CAP_NET_ADMIN capability in any user or network namespace.

CVE-2023-6931

    Budimir Markovic reported a heap out-of-bounds write vulnerability
    in the Linux kernel's Performance Events system which may result in
    denial of service or privilege escalation. The default settings in
    Debian prevent exploitation unless more permissive settings have
    been applied in the kernel.perf_event_paranoid sysctl.

CVE-2023-6932

    A use-after-free vulnerability in the IPv4 IGMP implementation may
    result in denial of service or privilege escalation.

CVE-2023-25775

    Ivan D Barrera, Christopher Bednarz, Mustafa Ismail and Shiraz
    Saleem discovered that improper access control in the Intel Ethernet
    Controller RDMA driver may result in privilege escalation.

CVE-2023-34324

    Marek Marczykowski-Gorecki reported a possible deadlock in the Xen
    guests event channel code which may allow a malicious guest
    administrator to cause a denial of service.

CVE-2023-35827

    Zheng Wang reported a use-after-free flaw in the Renesas Ethernet
    AVB support driver.

CVE-2023-45863

    A race condition in library routines for handling generic kernel
    objects may result in an out-of-bounds write in the
    fill_kobj_path() function.

CVE-2023-46813

    Tom Dohrmann reported that a race condition in the Secure Encrypted
    Virtualization (SEV) implementation when accessing MMIO registers
    may allow a local attacker in a SEV guest VM to cause a denial of
    service or potentially execute arbitrary code.

CVE-2023-46862

    It was discovered that a race condition in the io_uring
    subsystem may result in a NULL pointer dereference, causing a
    denial of service.

CVE-2023-51780

    It was discovered that a race condition in the ATM (Asynchronous
    Transfer Mode) subsystem may lead to a use-after-free.

CVE-2023-51781

    It was discovered that a race condition in the Appletalk subsystem
    may lead to a use-after-free.

CVE-2023-51782

    It was discovered that a race condition in the Amateur Radio X.25
    PLP (Rose) support may lead to a use-after-free. This module is not
    auto-loaded on Debian systems, so this issue only affects systems
    where it is explicitly loaded.
