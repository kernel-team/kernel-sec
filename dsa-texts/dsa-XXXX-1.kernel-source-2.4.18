--------------------------------------------------------------------------
Debian Security Advisory DSA 10XX-1                    security@debian.org
http://www.debian.org/security/               Martin Schulze, Dann Frazier
May 20th, 2006                          http://www.debian.org/security/faq
--------------------------------------------------------------------------

Package        : kernel-source-2.4.18,kernel-image-2.4.18-1-alpha,kernel-image-2.4.18-1-i386,kernel-image-2.4.18-hppa,kernel-image-2.4.18-powerpc-xfs,kernel-patch-2.4.18-powerpc,kernel-patch-benh
Vulnerability  : several
Problem-Type   : local/remote
Debian-specific: no
CVE IDs        : CVE-2004-0427 CVE-2005-0489 CVE-2004-0394 CVE-2004-0447 CVE-2004-0554 CVE-2004-0565 CVE-2004-0685  CVE-2005-0001 CVE-2004-0883 CVE-2004-0949 CVE-2004-1016 CVE-2004-1333 CVE-2004-0997 CVE-2004-1335 CVE-2004-1017 CVE-2005-0124 CVE-2005-0528 CVE-2003-0984 CVE-2004-1070 CVE-2004-1071 CVE-2004-1072 CVE-2004-1073 CVE-2004-1074 CVE-2004-0138 CVE-2004-1068 CVE-2004-1234 CVE-2005-0003 CVE-2004-1235 CVE-2005-0504 CVE-2005-0384 CVE-2005-0135

Several local and remote vulnerabilities have been discovered in the Linux
kernel that may lead to a denial of service or the execution of arbitrary
code. The Common Vulnerabilities and Exposures project identifies the
following problems:


 CVE-2004-0427

     A local denial of service vulnerability in do_fork() has been found.     

 CVE-2005-0489

     A local denial of service vulnerability in proc memory handling has
     been found.

 CVE-2004-0394

     A buffer overflow in the panic handling code has been found.

 CVE-2004-0447

     A local denial of service vulnerability through a null pointer
     dereference in the IA64 process handling code has been found.

 CVE-2004-0554

     A local denial of service vulnerability through an infinite loop in
     the signal handler code has been found.

 CVE-2004-0565

     An information leak in the context switch code has been found on
     the IA64 architecture.

 CVE-2004-0685

     Unsafe use of copy_to_user in USB drivers may disclose sensitive
     information.

 CVE-2005-0001

     A race condition in the i386 page fault handler may allow privilege
     escalation.

 CVE-2004-0883

     Multiple vulnerabilities in the SMB filesystem code may allow denial
     of service of information disclosure.

 CVE-2004-0949

     An information leak discovered in the SMB filesystem code.

 CVE-2004-1016

     A local denial of service vulnerability has been found in the SCM layer.

 CVE-2004-1333

     An integer overflow in the terminal code may allow a local denial of
     service vulnerability.

 CVE-2004-0997

     A local privilege escalation in the MIPS assembly code has been found.
 
 CVE-2004-1335
 
     A memory leak in the ip_options_get() function may lead to denial of
     service.
      
 CVE-2004-1017

     Multiple overflows exist in the io_edgeport driver which might be usable
     as a denial of service attack vector.
 
 CVE-2005-0124

     Bryan Fulton reported a bounds checking bug in the coda_pioctl function
     which may allow local users to execute arbitrary code or trigger a denial
     of service attack.

 CVE-2005-0528

     A local privilege escalation in the mremap function has been found

 CVE-2003-0984

     Inproper initialization of the RTC may disclose information.

 CVE-2004-1070

     Insufficient input sanitising in the load_elf_binary() function may
     lead to privilege escalation.

 CVE-2004-1071

     Incorrect error handling in the binfmt_elf loader may lead to privilege
     escalation.

 CVE-2004-1072

     A buffer overflow in the binfmt_elf loader may lead to privilege
     escalation or denial of service.

 CVE-2004-1073

     The open_exec function may disclose information.

 CVE-2004-1074

     The binfmt code is vulnerable to denial of service through malformed
     a.out binaries.

 CVE-2004-0138

     A denial of service vulnerability in the ELF loader has been found.

 CVE-2004-1068

     A programming error in the unix_dgram_recvmsg() function may lead to
     privilege escalation.

 CVE-2004-1234

     The ELF loader is vulnerable to denial of service through malformed
     binaries.

 CVE-2005-0003

     Crafted ELF binaries may lead to privilege escalation, due to 
     insufficient checking of overlapping memory regions.

 CVE-2004-1235

     A race condition in the load_elf_library() and binfmt_aout() functions
     may allow privilege escalation.

 CVE-2005-0504

     An integer overflow in the Moxa driver may lead to privilege escalation.

 CVE-2005-0384

     A remote denial of service vulnerability has been found in the PPP
     driver.

 CVE-2005-0135

     An IA64 specific local denial of service vulnerability has been found
     in the unw_unwind_to_user() function.

The following matrix explains which kernel version for which architecture
fix the problems mentioned above:

                                     Debian 3.0 (woody)
     Source                          2.4.18-14.4
     Alpha architecture              2.4.18-15woody1
     Intel IA-32 architecture        2.4.18-13.2
     HP Precision architecture       62.4 
     PowerPC architecture            2.4.18-1woody6
     PowerPC architecture/XFS        20020329woody1            
     PowerPC architecture/benh       20020304woody1
     Sun Sparc architecture          

We recommend that you upgrade your kernel package immediately and reboot
the machine.

Upgrade Instructions
--------------------

wget url
        will fetch the file for you
dpkg -i file.deb
        will install the referenced file.

If you are using the apt-get package manager, use the line for
sources.list as given below:

apt-get update
        will update the internal database
apt-get dist-upgrade
        will install corrected packages

You may use an automated update by adding the resources from the
footer to the proper configuration.


Debian GNU/Linux 3.0 alias woody
--------------------------------


  These files will probably be moved into the stable distribution on
  its next update.

---------------------------------------------------------------------------------
For apt-get: deb http://security.debian.org/ stable/updates main
For dpkg-ftp: ftp://security.debian.org/debian-security dists/stable/updates/main
Mailing list: debian-security-announce@lists.debian.org
Package info: `apt-cache show <pkg>' and http://packages.debian.org/<pkg>
