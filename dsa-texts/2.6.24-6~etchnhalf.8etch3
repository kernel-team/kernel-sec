----------------------------------------------------------------------
Debian Security Advisory DSA-1864-1                security@debian.org
http://www.debian.org/security/                           Dann Frazier
Aug 16, 2009                        http://www.debian.org/security/faq
----------------------------------------------------------------------

Package        : linux-2.6.24
Vulnerability  : privilege escalation
Problem type   : local
Debian-specific: no
CVE Id(s)      : CVE-2009-2692

A vulnerability has been discovered in the Linux kernel that may lead
to privilege escalation. The Common Vulnerabilities and Exposures
project identifies the following problem:

CVE-2009-2692

    Tavis Ormandy and Julien Tinnes discovered an issue with how the
    sendpage function is initialized in the proto_ops structure.
    Local users can exploit this vulnerability to gain elevated
    privileges.

For the oldstable distribution (etch), this problem has been fixed in
version 2.6.24-6~etchnhalf.8etch3.

We recommend that you upgrade your linux-2.6.24 packages.

Note: Debian 'etch' includes linux kernel packages based upon both the
2.6.18 and 2.6.24 linux releases.  All known security issues are
carefully tracked against both packages and both packages will receive
security updates until security support for Debian 'etch'
concludes. However, given the high frequency at which low-severity
security issues are discovered in the kernel and the resource
requirements of doing an update, lower severity 2.6.18 and 2.6.24
updates will typically release in a staggered or "leap-frog" fashion.

Upgrade instructions
--------------------

wget url
        will fetch the file for you
dpkg -i file.deb
        will install the referenced file.

If you are using the apt-get package manager, use the line for
sources.list as given below:

apt-get update
        will update the internal database
apt-get upgrade
        will install corrected packages

You may use an automated update by adding the resources from the
footer to the proper configuration.

Debian GNU/Linux 4.0 alias etch
-------------------------------

Oldstable updates are available for alpha, amd64, hppa, i386, ia64, mips, mipsel, powerpc, s390 and sparc.

The arm update will be released once the build becomes available.

Source archives:

  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-2.6.24_2.6.24-6~etchnhalf.8etch3.dsc
    Size/MD5 checksum:     5117 260db0dd510bc8ae520d70d8f2d777a7
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-2.6.24_2.6.24-6~etchnhalf.8etch3.diff.gz
    Size/MD5 checksum:  4042082 086b8b219adb642aea83d54aff143ca4
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-2.6.24_2.6.24.orig.tar.gz
    Size/MD5 checksum: 59630522 6b8751d1eb8e71498ba74bbd346343af

Architecture independent packages:

  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-support-2.6.24-etchnhalf.1_2.6.24-6~etchnhalf.8etch3_all.deb
    Size/MD5 checksum:    97098 e4397c771b232a614bb9a71bedcdbb95
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-patch-debian-2.6.24_2.6.24-6~etchnhalf.8etch3_all.deb
    Size/MD5 checksum:   932316 e2a6efbb1a3efbfead7ed4c0ce505b07
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-manual-2.6.24_2.6.24-6~etchnhalf.8etch3_all.deb
    Size/MD5 checksum:  1544288 aa3d7bda9d030128966127256dcbcee2
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-source-2.6.24_2.6.24-6~etchnhalf.8etch3_all.deb
    Size/MD5 checksum: 46863740 a61a335af22645db849cd8eb505ac0af
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-tree-2.6.24_2.6.24-6~etchnhalf.8etch3_all.deb
    Size/MD5 checksum:    82706 155fbfde7a84b13d3ec47e736974417f
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-doc-2.6.24_2.6.24-6~etchnhalf.8etch3_all.deb
    Size/MD5 checksum:  4262452 a52a4d41a03e278f55b4a8a25d9ef4a8

alpha architecture (DEC Alpha)

  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-all-alpha_2.6.24-6~etchnhalf.8etch3_alpha.deb
    Size/MD5 checksum:    82304 48ea456ff4fe13e7f31da69a7dc35ba0
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-alpha-smp_2.6.24-6~etchnhalf.8etch3_alpha.deb
    Size/MD5 checksum:   328286 f16d82a2cca45c9f72c54e0089c525f4
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-image-2.6.24-etchnhalf.1-alpha-generic_2.6.24-6~etchnhalf.8etch3_alpha.deb
    Size/MD5 checksum: 26639542 32dd7c467e6d7587535cfe64931ceb0c
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-common_2.6.24-6~etchnhalf.8etch3_alpha.deb
    Size/MD5 checksum:  3453506 3fdb9082af544d607c7a88617184070c
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-all_2.6.24-6~etchnhalf.8etch3_alpha.deb
    Size/MD5 checksum:    82282 f1556a2654ba07d621db8852a51360b7
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-image-2.6.24-etchnhalf.1-alpha-legacy_2.6.24-6~etchnhalf.8etch3_alpha.deb
    Size/MD5 checksum: 26620936 cb6d170f1316497a71457ae69d027087
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-image-2.6.24-etchnhalf.1-alpha-smp_2.6.24-6~etchnhalf.8etch3_alpha.deb
    Size/MD5 checksum: 27236202 8b75fadab4d61c0b091f7c1d3d2e49e9
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-alpha-generic_2.6.24-6~etchnhalf.8etch3_alpha.deb
    Size/MD5 checksum:   328794 fe985a00812d5722de7526c4d1f4bf84
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-alpha-legacy_2.6.24-6~etchnhalf.8etch3_alpha.deb
    Size/MD5 checksum:   329320 d9699090b6933f916b6c2eca8e49ad29

amd64 architecture (AMD x86_64 (AMD64))

  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-all-amd64_2.6.24-6~etchnhalf.8etch3_amd64.deb
    Size/MD5 checksum:    82286 67fa656db0c4092cf739d76eed1bfecc
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-all_2.6.24-6~etchnhalf.8etch3_amd64.deb
    Size/MD5 checksum:    82276 c48f75bff09eda0fd6b54933fa404a56
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-amd64_2.6.24-6~etchnhalf.8etch3_amd64.deb
    Size/MD5 checksum:   351486 2d670f87730c8ee82dbad8bd8e4aa8e1
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-common_2.6.24-6~etchnhalf.8etch3_amd64.deb
    Size/MD5 checksum:  3649960 c086b2b3e5fe7879d5ad6ece75432217
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-image-2.6.24-etchnhalf.1-amd64_2.6.24-6~etchnhalf.8etch3_amd64.deb
    Size/MD5 checksum: 19596054 f845d6a7a4fd0d28ea33b6d63a6330ec

hppa architecture (HP PA RISC)

  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-parisc_2.6.24-6~etchnhalf.8etch3_hppa.deb
    Size/MD5 checksum:   257904 a335adccfce330f7530f4d3171af239a
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-all_2.6.24-6~etchnhalf.8etch3_hppa.deb
    Size/MD5 checksum:    82394 ede43b5dacf5167108c7ed0492396b2c
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-parisc64-smp_2.6.24-6~etchnhalf.8etch3_hppa.deb
    Size/MD5 checksum:   261484 f20a75d9a1c4b1bbf01388c5fc98e857
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-parisc-smp_2.6.24-6~etchnhalf.8etch3_hppa.deb
    Size/MD5 checksum:   259236 2413640c034f1dad4633e200ec418072
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-image-2.6.24-etchnhalf.1-parisc-smp_2.6.24-6~etchnhalf.8etch3_hppa.deb
    Size/MD5 checksum: 13846528 27fc8f45cd23bf9994cd85962cf5a51f
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-image-2.6.24-etchnhalf.1-parisc64-smp_2.6.24-6~etchnhalf.8etch3_hppa.deb
    Size/MD5 checksum: 14830092 0c99c3fbc1be42a3b9cbde21b02aa8c5
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-image-2.6.24-etchnhalf.1-parisc64_2.6.24-6~etchnhalf.8etch3_hppa.deb
    Size/MD5 checksum: 14371780 9467f1cd2526b0d499e1b959e24f9d5a
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-image-2.6.24-etchnhalf.1-parisc_2.6.24-6~etchnhalf.8etch3_hppa.deb
    Size/MD5 checksum: 13334674 a13576b56cf65a4f894f274699854e99
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-all-hppa_2.6.24-6~etchnhalf.8etch3_hppa.deb
    Size/MD5 checksum:    82426 6f945889ca9841ffd19c91dadfdb8964
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-parisc64_2.6.24-6~etchnhalf.8etch3_hppa.deb
    Size/MD5 checksum:   258552 043990f20a47d13eb25591d8e081331b
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-common_2.6.24-6~etchnhalf.8etch3_hppa.deb
    Size/MD5 checksum:  3444704 8e355b36e48d0ffdac0aa71f0d030f5e

i386 architecture (Intel ia32)

  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-image-2.6.24-etchnhalf.1-686_2.6.24-6~etchnhalf.8etch3_i386.deb
    Size/MD5 checksum: 19146260 73cad50fc44d2f19ffaa900316bed9e3
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-all_2.6.24-6~etchnhalf.8etch3_i386.deb
    Size/MD5 checksum:    82280 89181f47e3a8883c03564bfe8957da3f
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-image-2.6.24-etchnhalf.1-486_2.6.24-6~etchnhalf.8etch3_i386.deb
    Size/MD5 checksum: 19214624 60a9a5abd25ed8aa70a4de2ccefffac8
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-amd64_2.6.24-6~etchnhalf.8etch3_i386.deb
    Size/MD5 checksum:   345458 471a4cb3d09fe283abcafd73238d764f
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-486_2.6.24-6~etchnhalf.8etch3_i386.deb
    Size/MD5 checksum:   358316 ffb432647ba9dab089f01415462f00ab
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-all-i386_2.6.24-6~etchnhalf.8etch3_i386.deb
    Size/MD5 checksum:    82308 42fc4e3cad5367584e416044c35fb5c3
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-686-bigmem_2.6.24-6~etchnhalf.8etch3_i386.deb
    Size/MD5 checksum:   358276 828bedefd9e01638e9bdccf6b33f4bca
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-image-2.6.24-etchnhalf.1-686-bigmem_2.6.24-6~etchnhalf.8etch3_i386.deb
    Size/MD5 checksum: 19213418 9a805c1d1b196ce5fe15979906166a67
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-common_2.6.24-6~etchnhalf.8etch3_i386.deb
    Size/MD5 checksum:  3654924 914026225d7a9db8654fe867be056a56
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-image-2.6.24-etchnhalf.1-amd64_2.6.24-6~etchnhalf.8etch3_i386.deb
    Size/MD5 checksum: 19481352 65069cd170a0cd50b0156607147d0aa8
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-686_2.6.24-6~etchnhalf.8etch3_i386.deb
    Size/MD5 checksum:   357844 b84586e7aea801c1848f9dec083819bc

ia64 architecture (Intel ia64)

  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-image-2.6.24-etchnhalf.1-mckinley_2.6.24-6~etchnhalf.8etch3_ia64.deb
    Size/MD5 checksum: 32207604 402c41e93a56423fd8a2ca1f5a45fdb5
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-common_2.6.24-6~etchnhalf.8etch3_ia64.deb
    Size/MD5 checksum:  3567680 9f534a29c2825bf09b7c7abe221237d5
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-mckinley_2.6.24-6~etchnhalf.8etch3_ia64.deb
    Size/MD5 checksum:   318450 0daff5d269339488c0dd8619176bc2a3
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-image-2.6.24-etchnhalf.1-itanium_2.6.24-6~etchnhalf.8etch3_ia64.deb
    Size/MD5 checksum: 32024518 4347c987514070642678df44e636e6e9
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-itanium_2.6.24-6~etchnhalf.8etch3_ia64.deb
    Size/MD5 checksum:   318782 a90491d70a3d1372ffc19a85d080cc5f
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-all_2.6.24-6~etchnhalf.8etch3_ia64.deb
    Size/MD5 checksum:    82274 69eacc741e0191caf449234295178325
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-all-ia64_2.6.24-6~etchnhalf.8etch3_ia64.deb
    Size/MD5 checksum:    82292 2f6a2d2517ebce8dab965a79072d1e4e

mips architecture (MIPS (Big Endian))

  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-common_2.6.24-6~etchnhalf.8etch3_mips.deb
    Size/MD5 checksum:  3803746 d33e35dd57d46748eadcb875302c8830
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-image-2.6.24-etchnhalf.1-r4k-ip22_2.6.24-6~etchnhalf.8etch3_mips.deb
    Size/MD5 checksum: 10524412 8fcda23506a3091315dc505a70b48a50
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-all_2.6.24-6~etchnhalf.8etch3_mips.deb
    Size/MD5 checksum:    82282 605a74627588572521af7b1f7b4001d6
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-r4k-ip22_2.6.24-6~etchnhalf.8etch3_mips.deb
    Size/MD5 checksum:   214832 4209163f7a68fcda685d029e7d6d5550
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-image-2.6.24-etchnhalf.1-5kc-malta_2.6.24-6~etchnhalf.8etch3_mips.deb
    Size/MD5 checksum: 27775496 4a58ced0767c5dc46139cbc6a62fe289
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-image-2.6.24-etchnhalf.1-sb1a-bcm91480b_2.6.24-6~etchnhalf.8etch3_mips.deb
    Size/MD5 checksum: 17152512 abd5189a147a7e1cd867ed3c03e71ed4
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-image-2.6.24-etchnhalf.1-4kc-malta_2.6.24-6~etchnhalf.8etch3_mips.deb
    Size/MD5 checksum: 22151578 5141d8703554b90a7789a67eec8c0c38
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-all-mips_2.6.24-6~etchnhalf.8etch3_mips.deb
    Size/MD5 checksum:    82334 05ce6b210f6eea1abf780960a5fcf245
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-image-2.6.24-etchnhalf.1-r5k-ip32_2.6.24-6~etchnhalf.8etch3_mips.deb
    Size/MD5 checksum: 11958562 17f5524bbf172f519491dcc6fd6695f6
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-4kc-malta_2.6.24-6~etchnhalf.8etch3_mips.deb
    Size/MD5 checksum:   309540 17f9568a592621a408aabc76411f74f3
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-r5k-ip32_2.6.24-6~etchnhalf.8etch3_mips.deb
    Size/MD5 checksum:   225566 b8761b634e21bcf499cabbc95e84728f
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-5kc-malta_2.6.24-6~etchnhalf.8etch3_mips.deb
    Size/MD5 checksum:   308568 4a200a51e6ee1e31074195b229f3c5f1
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-image-2.6.24-etchnhalf.1-sb1-bcm91250a_2.6.24-6~etchnhalf.8etch3_mips.deb
    Size/MD5 checksum: 17167656 a09eed90b5a977c708953af1a5a5043c
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-sb1a-bcm91480b_2.6.24-6~etchnhalf.8etch3_mips.deb
    Size/MD5 checksum:   246448 411a3947488f439500177c937e2bff5a
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-sb1-bcm91250a_2.6.24-6~etchnhalf.8etch3_mips.deb
    Size/MD5 checksum:   247172 d8bb54b64e3f38de900dae75190df7ba

mipsel architecture (MIPS (Little Endian))

  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-image-2.6.24-etchnhalf.1-4kc-malta_2.6.24-6~etchnhalf.8etch3_mipsel.deb
    Size/MD5 checksum: 21734854 59f37977da0baae24a55c8eb315067b8
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-image-2.6.24-etchnhalf.1-5kc-malta_2.6.24-6~etchnhalf.8etch3_mipsel.deb
    Size/MD5 checksum: 26985416 115d4db9dad42497d847ebbf74e140b3
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-image-2.6.24-etchnhalf.1-r5k-cobalt_2.6.24-6~etchnhalf.8etch3_mipsel.deb
    Size/MD5 checksum: 13317246 6f8ebdd4c624de4e98e6371eb4c8a4c1
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-4kc-malta_2.6.24-6~etchnhalf.8etch3_mipsel.deb
    Size/MD5 checksum:   309376 eb5bf40d97e03af5fbd8c9890f8af93b
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-all_2.6.24-6~etchnhalf.8etch3_mipsel.deb
    Size/MD5 checksum:    82280 22d2215ccb5b64ac20bed5f178529aa2
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-sb1a-bcm91480b_2.6.24-6~etchnhalf.8etch3_mipsel.deb
    Size/MD5 checksum:   246244 89eff488b08be87402f1a45e9eabef9c
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-image-2.6.24-etchnhalf.1-sb1a-bcm91480b_2.6.24-6~etchnhalf.8etch3_mipsel.deb
    Size/MD5 checksum: 16630524 d7cfa60ba0ffbcd4971be8f88d667c41
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-r5k-cobalt_2.6.24-6~etchnhalf.8etch3_mipsel.deb
    Size/MD5 checksum:   245962 78cef56101225ba21738300cca0b3069
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-sb1-bcm91250a_2.6.24-6~etchnhalf.8etch3_mipsel.deb
    Size/MD5 checksum:   246178 b5a17e304541ecb44f624f87ee716436
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-all-mipsel_2.6.24-6~etchnhalf.8etch3_mipsel.deb
    Size/MD5 checksum:    82330 8418dc4a41b06af174635db9e558a5b0
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-5kc-malta_2.6.24-6~etchnhalf.8etch3_mipsel.deb
    Size/MD5 checksum:   309072 8086de86fe0fb6d385406a997abcd6f2
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-image-2.6.24-etchnhalf.1-sb1-bcm91250a_2.6.24-6~etchnhalf.8etch3_mipsel.deb
    Size/MD5 checksum: 16566932 dd3970de7d08559b4fbe8656763266fb
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-common_2.6.24-6~etchnhalf.8etch3_mipsel.deb
    Size/MD5 checksum:  3803644 1d87d405db12cd8eec5592ae6747c8b0

powerpc architecture (PowerPC)

  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-powerpc_2.6.24-6~etchnhalf.8etch3_powerpc.deb
    Size/MD5 checksum:   321592 731efe722218b9a0ef7785f508266613
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-powerpc-smp_2.6.24-6~etchnhalf.8etch3_powerpc.deb
    Size/MD5 checksum:   321120 50d2521c2155e5ad65eeb6056c71c859
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-image-2.6.24-etchnhalf.1-powerpc_2.6.24-6~etchnhalf.8etch3_powerpc.deb
    Size/MD5 checksum: 19298172 955f28c258cf919dc2d9b0de77f497e1
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-powerpc64_2.6.24-6~etchnhalf.8etch3_powerpc.deb
    Size/MD5 checksum:   322280 ec1bf4359bcde5c325b8184e34315a7c
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-all_2.6.24-6~etchnhalf.8etch3_powerpc.deb
    Size/MD5 checksum:    82280 153e8c449e73be4cdfaf5ae05f06baaa
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-all-powerpc_2.6.24-6~etchnhalf.8etch3_powerpc.deb
    Size/MD5 checksum:    82308 793ab7a8b591af583cdc9d2545408683
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-image-2.6.24-etchnhalf.1-powerpc64_2.6.24-6~etchnhalf.8etch3_powerpc.deb
    Size/MD5 checksum: 21243848 f5ed7fb0b2d8fcdc2f6668553c0c5981
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-image-2.6.24-etchnhalf.1-powerpc-smp_2.6.24-6~etchnhalf.8etch3_powerpc.deb
    Size/MD5 checksum: 19591696 8861fde385a36bb21c5ecef8a242a7fe
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-common_2.6.24-6~etchnhalf.8etch3_powerpc.deb
    Size/MD5 checksum:  3673152 bf2848280ec015877c8a7284f9416339
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-powerpc-miboot_2.6.24-6~etchnhalf.8etch3_powerpc.deb
    Size/MD5 checksum:   294222 6509367c6b1fa1bc127962d184b0b62c
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-image-2.6.24-etchnhalf.1-powerpc-miboot_2.6.24-6~etchnhalf.8etch3_powerpc.deb
    Size/MD5 checksum: 17550666 5c5f8728d022e5cac76504227cd88678

s390 architecture (IBM S/390)

  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-all_2.6.24-6~etchnhalf.8etch3_s390.deb
    Size/MD5 checksum:    82274 cffceebef5faf23241cd16a72b071d1a
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-common_2.6.24-6~etchnhalf.8etch3_s390.deb
    Size/MD5 checksum:  3429958 c879a280abbdc70635a55f90cf2a1e8e
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-s390x_2.6.24-6~etchnhalf.8etch3_s390.deb
    Size/MD5 checksum:   194660 89cbc2d88ff9046dc8e1709645082d95
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-image-2.6.24-etchnhalf.1-s390x_2.6.24-6~etchnhalf.8etch3_s390.deb
    Size/MD5 checksum:  7200800 8d71a71587ef35554bd3988c5e5d5202
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-image-2.6.24-etchnhalf.1-s390_2.6.24-6~etchnhalf.8etch3_s390.deb
    Size/MD5 checksum:  6950190 ea9b0b957b08ad9e7bbbe309e89d7c61
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-s390_2.6.24-6~etchnhalf.8etch3_s390.deb
    Size/MD5 checksum:   194118 d8ea7bfc62c1735f94080b7bd61fe8d6
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-image-2.6.24-etchnhalf.1-s390-tape_2.6.24-6~etchnhalf.8etch3_s390.deb
    Size/MD5 checksum:  1502154 c96930dde13957a2ff2819a3a37fbb95
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-all-s390_2.6.24-6~etchnhalf.8etch3_s390.deb
    Size/MD5 checksum:    82290 f843f09bde9f75caa5ae7b0367f1af80

sparc architecture (Sun SPARC/UltraSPARC)

  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-all-sparc_2.6.24-6~etchnhalf.8etch3_sparc.deb
    Size/MD5 checksum:    82420 a36527e8141eb908722bf6671a533dab
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-common_2.6.24-6~etchnhalf.8etch3_sparc.deb
    Size/MD5 checksum:  3651058 bf1ab4414bd95681a40fd9c0aa441562
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-image-2.6.24-etchnhalf.1-sparc64_2.6.24-6~etchnhalf.8etch3_sparc.deb
    Size/MD5 checksum: 13019868 4c7c6a236b664c38b7836bfa9edc6fc5
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-image-2.6.24-etchnhalf.1-sparc64-smp_2.6.24-6~etchnhalf.8etch3_sparc.deb
    Size/MD5 checksum: 13317370 bed1cbd52faee38fa6d8e4c9d0427ca3
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-sparc64_2.6.24-6~etchnhalf.8etch3_sparc.deb
    Size/MD5 checksum:   261684 b0b7035a0ca23831eae093ec02e40b0c
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-sparc64-smp_2.6.24-6~etchnhalf.8etch3_sparc.deb
    Size/MD5 checksum:   263590 4e744273bd4ef98940171bbe8b18c1b5
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-all_2.6.24-6~etchnhalf.8etch3_sparc.deb
    Size/MD5 checksum:    82396 918dd5f9238db38c4b53a0d48bb08355

  These changes will probably be included in the oldstable distribution on
  its next update.

---------------------------------------------------------------------------------
For apt-get: deb http://security.debian.org/ stable/updates main
For dpkg-ftp: ftp://security.debian.org/debian-security dists/stable/updates/main
Mailing list: debian-security-announce@lists.debian.org
Package info: `apt-cache show <pkg>' and http://packages.debian.org/<pkg>
