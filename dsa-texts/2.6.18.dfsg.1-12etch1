--------------------------------------------------------------------------
Debian Security Advisory DSA XXX-1                     security@debian.org
http://www.debian.org/security/                               Dann Frazier
XXXXX 8th, 2007                         http://www.debian.org/security/faq
--------------------------------------------------------------------------

Package        : linux-2.6
Vulnerability  : several
Problem-Type   : local/remote
Debian-specific: no
CVE ID         : CVE-2007-0005 CVE-2007-0958 CVE-2007-1357 CVE-2007-1592

Several local and remote vulnerabilities have been discovered in the Linux
kernel that may lead to a denial of service or the execution of arbitrary
code. The Common Vulnerabilities and Exposures project identifies the
following problems:

CVE-2007-0005

    Daniel Roethlisberger discovered two buffer overflows in the cm4040
    driver for the Omnikey CardMan 4040 device. A local user or malicious
    device could exploit this to execute arbitrary code in kernel space.

CVE-2007-0958

    Santosh Eraniose reported a vulnerability that allows local users to read
    otherwise unreadable files by triggering a core dump while using PT_INTERP.
    This is related to CVE-2004-1073.

CVE-2007-1357

    Jean Delvare reported a vulnerability in the appletalk subsystem.
    Systems with the appletalk module loaded can be triggered to crash
    by other systems on the local network via a malformed frame.

CVE-2007-1592

    Masayuki Nakagawa discovered that flow labels were inadvertently
    being shared between listening sockets and child sockets. This defect
    can be exploited by local users to cause a DoS (Oops).

This problem has been fixed in the stable distribution in version 
2.6.18.dfsg.1-12etch1.

The following matrix lists additional packages that were rebuilt for
compatibility with or to take advantage of this update:

                                 Debian 4.0 (etch)
     fai-kernels                 1.17etch1
     user-mode-linux             2.6.18-1um-2etch1

We recommend that you upgrade your kernel package immediately and reboot
the machine. If you have built a custom kernel from the kernel source
package, you will need to rebuild to take advantage of these fixes.

Upgrade Instructions
--------------------

wget url
        will fetch the file for you
dpkg -i file.deb
        will install the referenced file.

If you are using the apt-get package manager, use the line for
sources.list as given below:

apt-get update
        will update the internal database
apt-get upgrade
        will install corrected packages

You may use an automated update by adding the resources from the
footer to the proper configuration.


Debian GNU/Linux 4.0 alias etch
--------------------------------


  These files will probably be moved into the stable distribution on
  its next update.

---------------------------------------------------------------------------------
For apt-get: deb http://security.debian.org/ etch/updates main
For dpkg-ftp: ftp://security.debian.org/debian-security dists/etch/updates/main
Mailing list: debian-security-announce@lists.debian.org
Package info: `apt-cache show <pkg>' and http://packages.debian.org/<pkg>
