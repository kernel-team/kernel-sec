----------------------------------------------------------------------
Debian Security Advisory DSA-2094-1                security@debian.org
http://www.debian.org/security/                           dann frazier
August 19, 2010                     http://www.debian.org/security/faq
----------------------------------------------------------------------

Package        : linux-2.6
Vulnerability  : privilege escalation/denial of service/information leak
Problem type   : local
Debian-specific: no
CVE Id(s)      : CVE-2009-4895 CVE-2010-2226 CVE-2010-2240 CVE-2010-2248
                 CVE-2010-2521 CVE-2010-2798 CVE-2010-2803 CVE-2010-2959
                 CVE-2010-3015
Debian Bug(s)  : 589179
                 
Several vulnerabilities have been discovered in the Linux kernel that
may lead to a denial of service or privilege escalation. The Common
Vulnerabilities and Exposures project identifies the following problems:

CVE-2009-4895

    Kyle Bader reported an issue in the tty subsystem that allows local
    users to create a denial of service (NULL pointer dereference).

CVE-2010-2226

    Dan Rosenberg reported an issue in the xfs filesystem that allows local
    users to copy and read a file owned by another user, for which they
    only have write permissions, due to a lack of permission checking in the
    XFS_SWAPEXT ioctl.

CVE-2010-2240

    Rafal Wojtczuk reported an issue that allows users to obtain escalated
    privileges. Users must already have sufficient privileges to execute or
    connect clients to an Xorg server.

CVE-2010-2248

    Suresh Jayaraman discovered an issue in the CIFS filesystem. A malicious
    file server can set an incorrect "CountHigh" value, resulting in a
    denial of service (BUG_ON() assertion).

CVE-2010-2521

    Neil Brown reported an issue in the NFSv4 server code. A malicious client
    could trigger a denial of service (Oops) on a server due to a bug in
    the read_buf() routine.

CVE-2010-2798

    Bob Peterson reported an issue in the GFS2 file system. A file system
    user could cause a denial of service (Oops) via certain rename
    operations.

CVE-2010-2803

    Kees Cook reported an issue in the DRM (Direct Rendering Manager)
    subsystem. Local users with sufficient privileges (local X users
    or members of the 'video' group on a default Debian install) could
    acquire access to sensitive kernel memory.

CVE-2010-2959

    Ben Hawkes discovered an issue in the AF_CAN socket family. An integer
    overflow condition may allow local users to obtain elevated privileges.

CVE-2010-3015

    Toshiyuki Okajima reported an issue in the ext4 filesystem. Local users
    could trigger a denial of service (BUG assertion) by generating a specific
    set of filesystem operations.

This update also includes fixes a regression introduced by a previous
update. See the referenced Debian bug page for details.

For the stable distribution (lenny), this problem has been fixed in
version 2.6.26-24lenny1.

We recommend that you upgrade your linux-2.6 and user-mode-linux
packages.

The following matrix lists additional source packages that were
rebuilt for compatibility with or to take advantage of this update:

                                             Debian 5.0 (lenny)
     user-mode-linux                         2.6.26-1um-2+24lenny1

Upgrade instructions
--------------------

wget url
        will fetch the file for you
dpkg -i file.deb
        will install the referenced file.

If you are using the apt-get package manager, use the line for
sources.list as given below:

apt-get update
        will update the internal database
apt-get upgrade
        will install corrected packages

You may use an automated update by adding the resources from the
footer to the proper configuration.

Debian GNU/Linux 5.0 alias lenny
--------------------------------

Stable updates are available for alpha, amd64, armel, hppa, i386, ia64, mipsel, powerpc, s390 and sparc.
Updates for arm and mips will be released as they become available.

Source archives:

  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-2.6_2.6.26-24lenny1.dsc
    Size/MD5 checksum:     5778 0ce8e36117eece3c4b469d73be862cd3
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-2.6_2.6.26.orig.tar.gz
    Size/MD5 checksum: 61818969 85e039c2588d5bf3cb781d1c9218bbcb
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-2.6_2.6.26-24lenny1.diff.gz
    Size/MD5 checksum:  7952972 d3496a509cd9024910b5ee2cad4b5c70

Architecture independent packages:

  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-source-2.6.26_2.6.26-24lenny1_all.deb
    Size/MD5 checksum: 48766186 ae5653c62cd9e1631c02af9ebab6a93d
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-doc-2.6.26_2.6.26-24lenny1_all.deb
    Size/MD5 checksum:  4630140 1ae9b5193a604a5943cbe3580d5f8191
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-tree-2.6.26_2.6.26-24lenny1_all.deb
    Size/MD5 checksum:   111898 f367960b308b8261b7aa3d0f25e11038
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-patch-debian-2.6.26_2.6.26-24lenny1_all.deb
    Size/MD5 checksum:  2928174 3cb46b25f4861f1a9ce3adbb2625cd2e
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-support-2.6.26-2_2.6.26-24lenny1_all.deb
    Size/MD5 checksum:   127208 05cd21e0aa9a24437d64a5b69eb8b164
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-manual-2.6.26_2.6.26-24lenny1_all.deb
    Size/MD5 checksum:  1770504 6a3b2afc69ae07acd7d8e04777cf536e

alpha architecture (DEC Alpha)

  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-alpha-smp_2.6.26-24lenny1_alpha.deb
    Size/MD5 checksum: 29376452 e3e92c5a7a1b2c8c61ab186021845db4
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-alpha-generic_2.6.26-24lenny1_alpha.deb
    Size/MD5 checksum:   373854 6c65ed0a031d4745c24f1ec2b7f276cd
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-alpha-legacy_2.6.26-24lenny1_alpha.deb
    Size/MD5 checksum:   374266 6146a097a56eadef1c2dc4a2a5fca9af
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-libc-dev_2.6.26-24lenny1_alpha.deb
    Size/MD5 checksum:   761678 409b6d87222bb24bcc44e7f9c21a3acb
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-alpha-legacy_2.6.26-24lenny1_alpha.deb
    Size/MD5 checksum: 28661146 de9a99261590f5d0552a55d1fab03346
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-all_2.6.26-24lenny1_alpha.deb
    Size/MD5 checksum:   111400 d17b474a58cd72d20bb819009585a2af
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-alpha-smp_2.6.26-24lenny1_alpha.deb
    Size/MD5 checksum:   375414 edbcfa2ac1c469cb645986ae9ec141bf
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-common_2.6.26-24lenny1_alpha.deb
    Size/MD5 checksum:  3621248 d5d5c0a52e464e78470b7edf151d07df
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-alpha-generic_2.6.26-24lenny1_alpha.deb
    Size/MD5 checksum: 28676408 baef30b53e88fd8d0f64b733785f37df
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-all-alpha_2.6.26-24lenny1_alpha.deb
    Size/MD5 checksum:   111422 3b78b0fa08d981c7d2e6131ab22981d5

amd64 architecture (AMD x86_64 (AMD64))

  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-openvz-amd64_2.6.26-24lenny1_amd64.deb
    Size/MD5 checksum: 21103502 4c7103c1f85326b535f7610fcd50d3cc
  http://security.debian.org/pool/updates/main/l/linux-2.6/xen-linux-system-2.6.26-2-xen-amd64_2.6.26-24lenny1_amd64.deb
    Size/MD5 checksum:   111310 e30084d0f8937bd268f233017c2379f7
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-common-xen_2.6.26-24lenny1_amd64.deb
    Size/MD5 checksum:  3857776 375a98d09b469044d531dec064b261c8
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-vserver-amd64_2.6.26-24lenny1_amd64.deb
    Size/MD5 checksum: 20951994 d3efdf68c34c11e092074b7a9288412b
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-common-vserver_2.6.26-24lenny1_amd64.deb
    Size/MD5 checksum:  3758270 4819dc1bf63d7b2f45dae9c97862e856
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-amd64_2.6.26-24lenny1_amd64.deb
    Size/MD5 checksum: 20932872 5d1c202e4b8c2342dcb2e22acf5da88f
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-amd64_2.6.26-24lenny1_amd64.deb
    Size/MD5 checksum:   392096 ea443d7588fd206128e1d76b0f39dd19
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-xen-amd64_2.6.26-24lenny1_amd64.deb
    Size/MD5 checksum:  1810486 73c4556902bcd3d30ea1a28863fb10fb
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-all_2.6.26-24lenny1_amd64.deb
    Size/MD5 checksum:   111334 12012442629435325e5f3d405b0d3bd3
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-xen-amd64_2.6.26-24lenny1_amd64.deb
    Size/MD5 checksum:   388334 eb9f3a964b600de629d33f853e3a3196
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-vserver-amd64_2.6.26-24lenny1_amd64.deb
    Size/MD5 checksum:   392838 6bd3feb0aabd50629ffba604fbd042b1
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-all-amd64_2.6.26-24lenny1_amd64.deb
    Size/MD5 checksum:   111358 1ed5b299a0891367bcb63f9168847d5d
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-modules-2.6.26-2-xen-amd64_2.6.26-24lenny1_amd64.deb
    Size/MD5 checksum: 19315568 8ea2c004b2fe006bfe960536cfa50e15
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-libc-dev_2.6.26-24lenny1_amd64.deb
    Size/MD5 checksum:   754156 5a4bc9d3cfaa8ced91dc95db825183dc
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-openvz-amd64_2.6.26-24lenny1_amd64.deb
    Size/MD5 checksum:   397852 b07d599a71361d924468bce039b8decc
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-common_2.6.26-24lenny1_amd64.deb
    Size/MD5 checksum:  3725698 77718405270c595091ed89b2691075ca
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-common-openvz_2.6.26-24lenny1_amd64.deb
    Size/MD5 checksum:  3781332 c4542267929cf31915d63890881eaa06

armel architecture (ARM EABI)

  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-ixp4xx_2.6.26-24lenny1_armel.deb
    Size/MD5 checksum:   366832 1f155d6b72628fdb5be899a39c265337
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-versatile_2.6.26-24lenny1_armel.deb
    Size/MD5 checksum:  9612934 ae77719bfd2c95ee9510686d8f1d78e2
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-libc-dev_2.6.26-24lenny1_armel.deb
    Size/MD5 checksum:   752166 faf17657c07f333b074de5bc24af824c
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-common_2.6.26-24lenny1_armel.deb
    Size/MD5 checksum:  4140220 b7e1181e9b79e46a8d8e0a2f22beb33c
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-iop32x_2.6.26-24lenny1_armel.deb
    Size/MD5 checksum: 12446598 693f1e3931c4719a652c465659edae10
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-all-armel_2.6.26-24lenny1_armel.deb
    Size/MD5 checksum:   111440 db0a1768222054d85d40282ffc7d92af
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-orion5x_2.6.26-24lenny1_armel.deb
    Size/MD5 checksum: 11407426 04c26d7e249045e644f3051809a37541
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-all_2.6.26-24lenny1_armel.deb
    Size/MD5 checksum:   111402 6417e82e224900aa4b13a3506b178a0f
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-orion5x_2.6.26-24lenny1_armel.deb
    Size/MD5 checksum:   363508 98bb4c45dc6e7f8e1b8eb556b74a8402
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-versatile_2.6.26-24lenny1_armel.deb
    Size/MD5 checksum:   338050 d1c99343c0c7c40988a759d827bacba5
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-iop32x_2.6.26-24lenny1_armel.deb
    Size/MD5 checksum:   369882 e188d441cedfccca00e786c0ceb03e15
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-ixp4xx_2.6.26-24lenny1_armel.deb
    Size/MD5 checksum: 11687998 2bb1a469fdc1278b0038f2f6804de4e7

hppa architecture (HP PA RISC)

  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-parisc64_2.6.26-24lenny1_hppa.deb
    Size/MD5 checksum: 17129864 baafdd8772c0c0132118526cbe567535
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-all_2.6.26-24lenny1_hppa.deb
    Size/MD5 checksum:   111332 c0dd370e1693ce3d1892540a639114e6
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-parisc64-smp_2.6.26-24lenny1_hppa.deb
    Size/MD5 checksum: 17679444 bb46a8c99d6259d4fed04341934d123b
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-libc-dev_2.6.26-24lenny1_hppa.deb
    Size/MD5 checksum:   763838 20621929594057f3c4c2d13612ab9622
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-parisc-smp_2.6.26-24lenny1_hppa.deb
    Size/MD5 checksum:   303962 9460579b718eb92e2526538843ce30b4
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-parisc-smp_2.6.26-24lenny1_hppa.deb
    Size/MD5 checksum: 16390876 05e13f1856382a6f3294e6a1ec24593a
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-all-hppa_2.6.26-24lenny1_hppa.deb
    Size/MD5 checksum:   111354 1eb14feaae6649cf7591f6273da4a43f
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-parisc64_2.6.26-24lenny1_hppa.deb
    Size/MD5 checksum:   303008 ec4b168709292e777576aa7832aeeb3f
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-parisc64-smp_2.6.26-24lenny1_hppa.deb
    Size/MD5 checksum:   304442 e6e5d1332354d737a013b8a8a45b2204
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-parisc_2.6.26-24lenny1_hppa.deb
    Size/MD5 checksum:   302122 c19456cfa805c5f40b4c58a4142cbede
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-parisc_2.6.26-24lenny1_hppa.deb
    Size/MD5 checksum: 15788256 488a818aca169729b017120fb3090092
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-common_2.6.26-24lenny1_hppa.deb
    Size/MD5 checksum:  3599664 32c7382b43219855a1e72df6de711e30

i386 architecture (Intel ia32)

  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-686_2.6.26-24lenny1_i386.deb
    Size/MD5 checksum:   408058 c02e25baedf92db3993ab4498ccde45f
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-common_2.6.26-24lenny1_i386.deb
    Size/MD5 checksum:  3814432 f43d12a4a2a22a12aad990e78f70dbda
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-vserver-686_2.6.26-24lenny1_i386.deb
    Size/MD5 checksum:   409672 dd396a41f54f4be93884a1798ab1186d
  http://security.debian.org/pool/updates/main/l/linux-2.6/xen-linux-system-2.6.26-2-xen-686_2.6.26-24lenny1_i386.deb
    Size/MD5 checksum:   111400 ee8402e1dc7e7eb06faba821f7524595
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-amd64_2.6.26-24lenny1_i386.deb
    Size/MD5 checksum:   397910 53b540af50920f48c45938139df32f0d
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-common-openvz_2.6.26-24lenny1_i386.deb
    Size/MD5 checksum:  3871808 786bda2af271cc61fbbf0037e3504899
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-all-i386_2.6.26-24lenny1_i386.deb
    Size/MD5 checksum:   111452 dbf1d7ecf88a28f100b1895c89427e9d
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-vserver-686_2.6.26-24lenny1_i386.deb
    Size/MD5 checksum: 20417246 e6fb44111a5e7803f4f862423628fe6e
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-686-bigmem_2.6.26-24lenny1_i386.deb
    Size/MD5 checksum: 20504930 14f869a0cdedc0e90cfb6370fcead9ab
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-libc-dev_2.6.26-24lenny1_i386.deb
    Size/MD5 checksum:   768268 0773eec5495ac3354793e28eae3dfd22
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-486_2.6.26-24lenny1_i386.deb
    Size/MD5 checksum: 20358790 b8212e3507fdeef0027e98897d389fb1
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-xen-686_2.6.26-24lenny1_i386.deb
    Size/MD5 checksum:  1597374 21ec33a79a7b31bd87b1f31ff81c577e
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-all_2.6.26-24lenny1_i386.deb
    Size/MD5 checksum:   111396 a59637d79ad91f1c6354d1bbe55b46f4
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-common-xen_2.6.26-24lenny1_i386.deb
    Size/MD5 checksum:  3952692 701f630559cdec0d532e496f57f248b2
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-openvz-686_2.6.26-24lenny1_i386.deb
    Size/MD5 checksum:   417032 39842bae4868093370baafca0cecdb81
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-vserver-686-bigmem_2.6.26-24lenny1_i386.deb
    Size/MD5 checksum:   409788 97536f91b740bb190dd047c951f1bc6d
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-686-bigmem_2.6.26-24lenny1_i386.deb
    Size/MD5 checksum:   408672 f5a90ff6326d98d3d7fbf596bd825119
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-xen-686_2.6.26-24lenny1_i386.deb
    Size/MD5 checksum:   396676 d6462e5b97856af92663ce7749427569
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-modules-2.6.26-2-xen-686_2.6.26-24lenny1_i386.deb
    Size/MD5 checksum: 18201864 77c0ddb0c695e3c19ae55e6c4142edf8
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-486_2.6.26-24lenny1_i386.deb
    Size/MD5 checksum:   407628 afe63eeca31e495d8b4c29ffb5f6a25a
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-common-vserver_2.6.26-24lenny1_i386.deb
    Size/MD5 checksum:  3848486 12a26b41b01625a1b6a9e3729992cefb
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-amd64_2.6.26-24lenny1_i386.deb
    Size/MD5 checksum: 21026658 6654240cbc442bd9055fc292d59c8d2c
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-vserver-686-bigmem_2.6.26-24lenny1_i386.deb
    Size/MD5 checksum: 20534848 9b6df06ea4f88178817cf18863bcb8db
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-686_2.6.26-24lenny1_i386.deb
    Size/MD5 checksum: 20389060 f61b782428da6b62e291579bd0025ba7
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-openvz-686_2.6.26-24lenny1_i386.deb
    Size/MD5 checksum: 20687876 78828d231f3ecdae22208b8eaf0930a6

ia64 architecture (Intel ia64)

  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-all_2.6.26-24lenny1_ia64.deb
    Size/MD5 checksum:   111324 7d6193883399cd69ff42b694d18e7866
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-mckinley_2.6.26-24lenny1_ia64.deb
    Size/MD5 checksum:   364724 f64038e0725778c9f0f9182637efb486
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-common-vserver_2.6.26-24lenny1_ia64.deb
    Size/MD5 checksum:  3761686 1d6c1f023dfff9b4b97f9c6ec838fff4
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-mckinley_2.6.26-24lenny1_ia64.deb
    Size/MD5 checksum: 34467638 9c87dddee6a74727405ebdd3f4fe24e6
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-common_2.6.26-24lenny1_ia64.deb
    Size/MD5 checksum:  3726890 83cd758c237fb1b69f97834121764d88
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-itanium_2.6.26-24lenny1_ia64.deb
    Size/MD5 checksum:   364660 78b97de6932a63dce9131d4d6af01ebb
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-vserver-mckinley_2.6.26-24lenny1_ia64.deb
    Size/MD5 checksum:   365386 3279b0e12707f771324dcfeb317e2bbd
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-vserver-itanium_2.6.26-24lenny1_ia64.deb
    Size/MD5 checksum:   365444 6626dad4525bf6db2b2fa1219a226e8d
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-itanium_2.6.26-24lenny1_ia64.deb
    Size/MD5 checksum: 34287774 0d3f7ced1551b7aa4f94227b8fd8865b
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-libc-dev_2.6.26-24lenny1_ia64.deb
    Size/MD5 checksum:   767876 e0011d5bae33329d57628ed36ae4843f
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-vserver-mckinley_2.6.26-24lenny1_ia64.deb
    Size/MD5 checksum: 34535048 ccd5812a90e7d10e0a2f342096ea1b34
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-vserver-itanium_2.6.26-24lenny1_ia64.deb
    Size/MD5 checksum: 34360056 5814e2ce359ccf4ac16bf24222a48aba
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-all-ia64_2.6.26-24lenny1_ia64.deb
    Size/MD5 checksum:   111360 6dd4112255bde572b6d3d5460985c270

mipsel architecture (MIPS (Little Endian))

  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-4kc-malta_2.6.26-24lenny1_mipsel.deb
    Size/MD5 checksum:   361008 d7f7bac1f9cfa9aed2c77486f520b2bb
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-sb1a-bcm91480b_2.6.26-24lenny1_mipsel.deb
    Size/MD5 checksum:   303814 3bf2bad693d0be377b83aa0e079f86fe
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-r5k-cobalt_2.6.26-24lenny1_mipsel.deb
    Size/MD5 checksum: 15057754 64290f78ef3844ad059f2bf609fcbc62
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-r5k-cobalt_2.6.26-24lenny1_mipsel.deb
    Size/MD5 checksum:   299036 e0d5abb6b94ff53cc8a0521901d484ab
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-5kc-malta_2.6.26-24lenny1_mipsel.deb
    Size/MD5 checksum: 28572436 a69f8a38481307a029b114050e19bf6e
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-sb1-bcm91250a_2.6.26-24lenny1_mipsel.deb
    Size/MD5 checksum:   305360 52e4efa84b29fee10f7ffae89f02d02c
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-5kc-malta_2.6.26-24lenny1_mipsel.deb
    Size/MD5 checksum:   359982 43ac553ed441843cc283c0bec3fa09a7
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-common_2.6.26-24lenny1_mipsel.deb
    Size/MD5 checksum:  3968770 eb5c43b64e93f57f6d1da814427e32a1
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-all-mipsel_2.6.26-24lenny1_mipsel.deb
    Size/MD5 checksum:   111454 257fe8ee57324c5ea448ecc32111c8f4
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-sb1a-bcm91480b_2.6.26-24lenny1_mipsel.deb
    Size/MD5 checksum: 19672236 5698f5c5bc3e006d6edc8b9619bd0a21
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-libc-dev_2.6.26-24lenny1_mipsel.deb
    Size/MD5 checksum:   761914 b7174d5d8f0f39830e28e1aba37dd2a5
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-sb1-bcm91250a_2.6.26-24lenny1_mipsel.deb
    Size/MD5 checksum: 19683202 a3e224b30fce10c6c5e31b8d8d5fcc46
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-all_2.6.26-24lenny1_mipsel.deb
    Size/MD5 checksum:   111400 52a74ce976723a0cbc141dd1e0db57fd
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-4kc-malta_2.6.26-24lenny1_mipsel.deb
    Size/MD5 checksum: 23075416 32735c6f2f6900b86070107a2d34b3f5

powerpc architecture (PowerPC)

  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-all_2.6.26-24lenny1_powerpc.deb
    Size/MD5 checksum:   111332 92155f9d1e9ec2ec31d6f33ac649136f
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-powerpc_2.6.26-24lenny1_powerpc.deb
    Size/MD5 checksum:   364992 cc97263f289d7ea2a0807c78929a203e
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-vserver-powerpc_2.6.26-24lenny1_powerpc.deb
    Size/MD5 checksum:   366428 79af89be2e8bf1da1492258a03b7598b
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-powerpc-smp_2.6.26-24lenny1_powerpc.deb
    Size/MD5 checksum:   366838 b32a9c691411843313e7ea39262d688d
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-powerpc64_2.6.26-24lenny1_powerpc.deb
    Size/MD5 checksum:   371814 d4a04a4638f9052965b361d698c4f1ec
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-vserver-powerpc64_2.6.26-24lenny1_powerpc.deb
    Size/MD5 checksum: 23476290 174feffe934fc8c79775093e250366fc
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-powerpc64_2.6.26-24lenny1_powerpc.deb
    Size/MD5 checksum: 23421134 14658df5bd840509070edecf32cbcde6
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-common_2.6.26-24lenny1_powerpc.deb
    Size/MD5 checksum:  3783748 d84a73888a9391bc5e70ce4945d5f5eb
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-vserver-powerpc_2.6.26-24lenny1_powerpc.deb
    Size/MD5 checksum: 23584926 9ae271e80ebb51d5464af2916fc11d35
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-common-vserver_2.6.26-24lenny1_powerpc.deb
    Size/MD5 checksum:  3816016 12009d46a98c1d27cb6e74573c296367
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-libc-dev_2.6.26-24lenny1_powerpc.deb
    Size/MD5 checksum:   747786 5a171d444c66c9264113d651ee0a63cf
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-powerpc-smp_2.6.26-24lenny1_powerpc.deb
    Size/MD5 checksum: 23559936 df39d3576d54e575c6c38373cd06e836
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-vserver-powerpc64_2.6.26-24lenny1_powerpc.deb
    Size/MD5 checksum:   372434 99b58c731369d34994ef44f274de65e1
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-all-powerpc_2.6.26-24lenny1_powerpc.deb
    Size/MD5 checksum:   111372 48880f588ab7661bbf252336b0681bff
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-powerpc_2.6.26-24lenny1_powerpc.deb
    Size/MD5 checksum: 23159418 722bae116201b5fc2f9b625add98f117
powerpc architecture (PowerPC)

  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-all_2.6.26-24lenny1_powerpc.deb
    Size/MD5 checksum:   111332 92155f9d1e9ec2ec31d6f33ac649136f
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-powerpc_2.6.26-24lenny1_powerpc.deb
    Size/MD5 checksum:   364992 cc97263f289d7ea2a0807c78929a203e
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-vserver-powerpc_2.6.26-24lenny1_powerpc.deb
    Size/MD5 checksum:   366428 79af89be2e8bf1da1492258a03b7598b
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-powerpc-smp_2.6.26-24lenny1_powerpc.deb
    Size/MD5 checksum:   366838 b32a9c691411843313e7ea39262d688d
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-powerpc64_2.6.26-24lenny1_powerpc.deb
    Size/MD5 checksum:   371814 d4a04a4638f9052965b361d698c4f1ec
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-vserver-powerpc64_2.6.26-24lenny1_powerpc.deb
    Size/MD5 checksum: 23476290 174feffe934fc8c79775093e250366fc
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-powerpc64_2.6.26-24lenny1_powerpc.deb
    Size/MD5 checksum: 23421134 14658df5bd840509070edecf32cbcde6
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-common_2.6.26-24lenny1_powerpc.deb
    Size/MD5 checksum:  3783748 d84a73888a9391bc5e70ce4945d5f5eb
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-vserver-powerpc_2.6.26-24lenny1_powerpc.deb
    Size/MD5 checksum: 23584926 9ae271e80ebb51d5464af2916fc11d35
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-common-vserver_2.6.26-24lenny1_powerpc.deb
    Size/MD5 checksum:  3816016 12009d46a98c1d27cb6e74573c296367
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-libc-dev_2.6.26-24lenny1_powerpc.deb
    Size/MD5 checksum:   747786 5a171d444c66c9264113d651ee0a63cf
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-powerpc-smp_2.6.26-24lenny1_powerpc.deb
    Size/MD5 checksum: 23559936 df39d3576d54e575c6c38373cd06e836
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-vserver-powerpc64_2.6.26-24lenny1_powerpc.deb
    Size/MD5 checksum:   372434 99b58c731369d34994ef44f274de65e1
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-all-powerpc_2.6.26-24lenny1_powerpc.deb
    Size/MD5 checksum:   111372 48880f588ab7661bbf252336b0681bff
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-powerpc_2.6.26-24lenny1_powerpc.deb
    Size/MD5 checksum: 23159418 722bae116201b5fc2f9b625add98f117

s390 architecture (IBM S/390)

  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-s390_2.6.26-24lenny1_s390.deb
    Size/MD5 checksum:   237374 0c54632182090dbb732a4a525fe053a6
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-all_2.6.26-24lenny1_s390.deb
    Size/MD5 checksum:   111396 832d2b93fdc6c61f23debaafea91c6bc
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-all-s390_2.6.26-24lenny1_s390.deb
    Size/MD5 checksum:   111422 9e70d40b8d7b1ce9da5196bf9dce484b
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-s390x_2.6.26-24lenny1_s390.deb
    Size/MD5 checksum:   237994 a7a0c60f5e9a839dc55b738c04718a76
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-common_2.6.26-24lenny1_s390.deb
    Size/MD5 checksum:  3599976 5f6c3089eb27f19df4abfd3d578615be
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-vserver-s390x_2.6.26-24lenny1_s390.deb
    Size/MD5 checksum:   238976 2e90cd43e3f3d7afbc90857e0cd83d5b
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-s390_2.6.26-24lenny1_s390.deb
    Size/MD5 checksum:  7536196 ccd69b4e1dbca68957825764977daaa2
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-vserver-s390x_2.6.26-24lenny1_s390.deb
    Size/MD5 checksum:  7890790 5f152ec3c04e24501ec36d8b2f655720
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-libc-dev_2.6.26-24lenny1_s390.deb
    Size/MD5 checksum:   759090 6c3081fa6449d8882fec69d7a8a8008e
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-s390x_2.6.26-24lenny1_s390.deb
    Size/MD5 checksum:  7830470 2c8dc2df0c2e2603059b72564be2eae7
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-s390-tape_2.6.26-24lenny1_s390.deb
    Size/MD5 checksum:  1634736 aa8f22d0e16366be0a826e9add7f28d3
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-common-vserver_2.6.26-24lenny1_s390.deb
    Size/MD5 checksum:  3634960 f5bbf9e19c373e9e8e245b205419004c

sparc architecture (Sun SPARC/UltraSPARC)

  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-sparc64_2.6.26-24lenny1_sparc.deb
    Size/MD5 checksum:   303878 69f3efb9f270ad005532e96d4c94c4ab
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-sparc64-smp_2.6.26-24lenny1_sparc.deb
    Size/MD5 checksum: 14612812 109ac9c90ab7ba5cbb0b8f9f16274c5a
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-vserver-sparc64_2.6.26-24lenny1_sparc.deb
    Size/MD5 checksum:   305662 1c7550a9489f35d0533f659c2ad2091a
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-sparc64-smp_2.6.26-24lenny1_sparc.deb
    Size/MD5 checksum:   305364 b4b521a87338f9c9e78650427a9c0638
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-libc-dev_2.6.26-24lenny1_sparc.deb
    Size/MD5 checksum:   801816 3870316a344e888e2a87d1f1956830cd
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-sparc64_2.6.26-24lenny1_sparc.deb
    Size/MD5 checksum: 14292734 5846d462eaf122b84770c365815f4971
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-common-vserver_2.6.26-24lenny1_sparc.deb
    Size/MD5 checksum:  3821630 a464bf5db1b30df5f791b5c89a4ede05
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-common_2.6.26-24lenny1_sparc.deb
    Size/MD5 checksum:  3786842 fcba40e7a99141831f02c894250ea44c
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-all_2.6.26-24lenny1_sparc.deb
    Size/MD5 checksum:   111330 baf31abf2f15540f6833e41e48c214ae
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.26-2-vserver-sparc64_2.6.26-24lenny1_sparc.deb
    Size/MD5 checksum: 14632894 63567ed0394beddecb2ec0ebe061bcf4
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.26-2-all-sparc_2.6.26-24lenny1_sparc.deb
    Size/MD5 checksum:   111348 d02a9e8002d59de0f7568caef3ec7d61


  These files will probably be moved into the stable distribution on
  its next update.

---------------------------------------------------------------------------------
For apt-get: deb http://security.debian.org/ stable/updates main
For dpkg-ftp: ftp://security.debian.org/debian-security dists/stable/updates/main
Mailing list: debian-security-announce@lists.debian.org
Package info: `apt-cache show <pkg>' and http://packages.debian.org/<pkg>
