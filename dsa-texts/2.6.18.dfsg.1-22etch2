----------------------------------------------------------------------
Debian Security Advisory DSA-1630-1                security@debian.org
http://www.debian.org/security/                           dann frazier
Aug 21, 2008                        http://www.debian.org/security/faq
----------------------------------------------------------------------

Package        : linux-2.6
Vulnerability  : denial of service/information leak
Problem type   : several
Debian-specific: no
CVE Id(s)      : CVE-2007-6282 CVE-2008-0598 CVE-2008-2729 CVE-2008-2812
                 CVE-2008-2826 CVE-2008-2931 CVE-2008-3272 CVE-2008-3275

Several vulnerabilities have been discovered in the Linux kernel that may
lead to a denial of service or arbitrary code execution. The Common
Vulnerabilities and Exposures project identifies the following
problems:

CVE-2007-6282

    Dirk Nehring discovered a vulnerability in the IPsec code that allows
    remote users to cause a denial of service by sending a specially crafted
    ESP packet.

CVE-2008-0598

    Tavis Ormandy discovered a vulnerability that allows local users to access
    uninitialized kernel memory, possibly leaking sensitive data. This issue
    is specific to the amd64-flavour kernel images.

CVE-2008-2729

    Andi Kleen discovered an issue where uninitialized kernel memory
    was being leaked to userspace during an exception. This issue may allow
    local users to gain access to sensitive data. Only the amd64-flavour
    Debian kernel images are affected.

CVE-2008-2812

    Alan Cox discovered an issue in multiple tty drivers that allows
    local users to trigger a denial of service (NULL pointer dereference)
    and possibly obtain elevated privileges.

CVE-2008-2826

    Gabriel Campana discovered an integer overflow in the sctp code that
    can be exploited by local users to cause a denial of service.

CVE-2008-2931

    Miklos Szeredi reported a missing privilege check in the do_change_type()
    function. This allows local, unprivileged users to change the properties
    of mount points.

CVE-2008-3272

    Tobias Klein reported a locally exploitable data leak in the
    snd_seq_oss_synth_make_info() function. This may allow local users
    to gain access to sensitive information.

CVE-2008-3275

    Zoltan Sogor discovered a coding error in the VFS that allows local users
    to exploit a kernel memory leak resulting in a denial of service.

For the stable distribution (etch), this problem has been fixed in
version 2.6.18.dfsg.1-22etch2.

We recommend that you upgrade your linux-2.6, fai-kernels, and
user-mode-linux packages.

Upgrade instructions
--------------------

wget url
        will fetch the file for you
dpkg -i file.deb
        will install the referenced file.

If you are using the apt-get package manager, use the line for
sources.list as given below:

apt-get update
        will update the internal database
apt-get upgrade
        will install corrected packages

The following matrix lists additional source packages that were rebuilt for
compatability with or to take advantage of this update:

                                             Debian 4.0 (etch)
     fai-kernels                             1.17+etch.22etch2
     user-mode-linux                         2.6.18-1um-2etch.22etch2

You may use an automated update by adding the resources from the
footer to the proper configuration.

Debian GNU/Linux 4.0 alias etch
-------------------------------

Stable updates are available for alpha, amd64, arm, hppa, i386, ia64, mips, mipsel, powerpc, s390 and sparc.

Source archives:

  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-2.6_2.6.18.dfsg.1-22etch2.diff.gz
    Size/MD5 checksum:  5378366 80a876fbcded8984ff47308cf2ece776
  http://security.debian.org/pool/updates/main/u/user-mode-linux/user-mode-linux_2.6.18-1um.orig.tar.gz
    Size/MD5 checksum:    14435 4d10c30313e11a24621f7218c31f3582
  http://security.debian.org/pool/updates/main/f/fai-kernels/fai-kernels_1.17+etch.22etch2.dsc
    Size/MD5 checksum:      740 080fa46e372743186c973658347ceee1
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-2.6_2.6.18.dfsg.1.orig.tar.gz
    Size/MD5 checksum: 52225460 6a1ab0948d6b5b453ea0fce0fcc29060
  http://security.debian.org/pool/updates/main/u/user-mode-linux/user-mode-linux_2.6.18-1um-2etch.22etch2.dsc
    Size/MD5 checksum:      892 30580beb633eb4806c40d659f552cd88
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-2.6_2.6.18.dfsg.1-22etch2.dsc
    Size/MD5 checksum:     5672 780d93329a4cedaad9d0539c1cc400ac
  http://security.debian.org/pool/updates/main/u/user-mode-linux/user-mode-linux_2.6.18-1um-2etch.22etch2.diff.gz
    Size/MD5 checksum:    18180 2b9bbd3f4bcc3852320fc60d6947607e
  http://security.debian.org/pool/updates/main/f/fai-kernels/fai-kernels_1.17+etch.22etch2.tar.gz
    Size/MD5 checksum:    56550 01fd54902e7ac7c3035c5176f1deae3c

Architecture independent packages:

  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-support-2.6.18-6_2.6.18.dfsg.1-22etch2_all.deb
    Size/MD5 checksum:  3718952 804c3adc75403db743f603fd1dc44fa2
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-source-2.6.18_2.6.18.dfsg.1-22etch2_all.deb
    Size/MD5 checksum: 41460250 9fda57df0d76f6300b162c0765a87e32
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-patch-debian-2.6.18_2.6.18.dfsg.1-22etch2_all.deb
    Size/MD5 checksum:  1632622 6d644a3a0502065b7a9faa9a7efdb8f4
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-manual-2.6.18_2.6.18.dfsg.1-22etch2_all.deb
    Size/MD5 checksum:  1087372 8ff719e7a81edaa38b5eca31a7aaee02
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-doc-2.6.18_2.6.18.dfsg.1-22etch2_all.deb
    Size/MD5 checksum:  3590476 b803de0252e4f027567c5c13db4afe0a
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-tree-2.6.18_2.6.18.dfsg.1-22etch2_all.deb
    Size/MD5 checksum:    55722 a4dd6ba664f85780bbe1ebf7f3a77b25

alpha architecture (DEC Alpha)

  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-alpha-legacy_2.6.18.dfsg.1-22etch2_alpha.deb
    Size/MD5 checksum: 23464338 df80e9f9afac5942ec8cd73c8fa2acd3
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-all_2.6.18.dfsg.1-22etch2_alpha.deb
    Size/MD5 checksum:    55150 c752933d6628e0ebe75dbe47f29ad4e2
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-alpha-generic_2.6.18.dfsg.1-22etch2_alpha.deb
    Size/MD5 checksum:   268952 128bea32232629d3255799ddc83da7b3
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-vserver-alpha_2.6.18.dfsg.1-22etch2_alpha.deb
    Size/MD5 checksum:   269648 7617546d15e940343cbe381dfecae010
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-vserver_2.6.18.dfsg.1-22etch2_alpha.deb
    Size/MD5 checksum:  3053804 f72f8a710f0a4da22bc8fd431d9195ee
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-alpha-smp_2.6.18.dfsg.1-22etch2_alpha.deb
    Size/MD5 checksum:   268368 5c248dbb5e31cc30d4148196b1f5cef0
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-all-alpha_2.6.18.dfsg.1-22etch2_alpha.deb
    Size/MD5 checksum:    55180 2a8a3e402015cf1908906180eb6f8a5f
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-alpha-smp_2.6.18.dfsg.1-22etch2_alpha.deb
    Size/MD5 checksum: 23846050 ba3b97267e75c3d8027627b2a3ece5fb
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-alpha-generic_2.6.18.dfsg.1-22etch2_alpha.deb
    Size/MD5 checksum: 23485982 9ea2f705c0a99080425de75c48c66904
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-alpha-legacy_2.6.18.dfsg.1-22etch2_alpha.deb
    Size/MD5 checksum:   269242 1edcca9164f4eecd392897903269c1e2
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6_2.6.18.dfsg.1-22etch2_alpha.deb
    Size/MD5 checksum:  3028978 253a048a7d3ccdf823b8e28b47e5f849

amd64 architecture (AMD x86_64 (AMD64))

  http://security.debian.org/pool/updates/main/f/fai-kernels/fai-kernels_1.17+etch.22etch2_amd64.deb
    Size/MD5 checksum:  5961854 67e6d9bddf32e6ff37f15a37f9aa2b04
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-all-amd64_2.6.18.dfsg.1-22etch2_amd64.deb
    Size/MD5 checksum:    55174 94bc1ab70d46a153205059b1ba2fbd9e
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-vserver-amd64_2.6.18.dfsg.1-22etch2_amd64.deb
    Size/MD5 checksum:   279498 bf0107981fe6a603a9b72be5e94190bb
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-amd64_2.6.18.dfsg.1-22etch2_amd64.deb
    Size/MD5 checksum: 16909690 0d7e343ec621b05fb565ed0c4b355c9e
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-vserver_2.6.18.dfsg.1-22etch2_amd64.deb
    Size/MD5 checksum:  3252530 97dc43477d90cae8fd6b4033781ce589
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-xen-amd64_2.6.18.dfsg.1-22etch2_amd64.deb
    Size/MD5 checksum:   277930 b719a1156f14a870b6f0fed9fcac0e26
  http://security.debian.org/pool/updates/main/l/linux-2.6/xen-linux-system-2.6.18-6-xen-vserver-amd64_2.6.18.dfsg.1-22etch2_amd64.deb
    Size/MD5 checksum:    55146 865ab31e1451d19f5374492c7b5cec02
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-all_2.6.18.dfsg.1-22etch2_amd64.deb
    Size/MD5 checksum:    55152 811c0ed6f63166536426b3a30a0ae6a0
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-xen-amd64_2.6.18.dfsg.1-22etch2_amd64.deb
    Size/MD5 checksum:  1652318 1e02b1e5c1315c6775efd5a3ebb8eba0
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6_2.6.18.dfsg.1-22etch2_amd64.deb
    Size/MD5 checksum:  3228660 b3491a01820796609c57ff0ca45cd991
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-xen-vserver-amd64_2.6.18.dfsg.1-22etch2_amd64.deb
    Size/MD5 checksum:  1684052 214c87ec095a47ddb16cb5a0386385d0
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-modules-2.6.18-6-xen-vserver-amd64_2.6.18.dfsg.1-22etch2_amd64.deb
    Size/MD5 checksum: 15370528 6429150ddfdc20c94f5dd9223ce1eefa
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-vserver-amd64_2.6.18.dfsg.1-22etch2_amd64.deb
    Size/MD5 checksum: 16955552 08dbcfaeb394f720be04b43584cc2dca
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-amd64_2.6.18.dfsg.1-22etch2_amd64.deb
    Size/MD5 checksum:   277636 0c1dd9ed04830cef72a2799682cdb373
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-xen-vserver_2.6.18.dfsg.1-22etch2_amd64.deb
    Size/MD5 checksum:  3422414 a66a5347a66c224513912f23df6e7811
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-xen_2.6.18.dfsg.1-22etch2_amd64.deb
    Size/MD5 checksum:  3397342 d69524c251013eca5cb9c2fc6ac49f08
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-xen-vserver-amd64_2.6.18.dfsg.1-22etch2_amd64.deb
    Size/MD5 checksum:   278800 f808d9f0b51671184647976e61673ef7
  http://security.debian.org/pool/updates/main/l/linux-2.6/xen-linux-system-2.6.18-6-xen-amd64_2.6.18.dfsg.1-22etch2_amd64.deb
    Size/MD5 checksum:    55126 dcc26a10c820fe676abf6d0a8f733190
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-modules-2.6.18-6-xen-amd64_2.6.18.dfsg.1-22etch2_amd64.deb
    Size/MD5 checksum: 15356684 ca635c5a74fde0c121ac8ad0dca7f3bc

arm architecture (ARM)

  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-ixp4xx_2.6.18.dfsg.1-22etch2_arm.deb
    Size/MD5 checksum:   240724 05944f6adaed3622fdbf4fa94d9e37c1
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-all_2.6.18.dfsg.1-22etch2_arm.deb
    Size/MD5 checksum:    55194 cfb4019acf8ce2b9eca9a06cde8fa6d8
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-rpc_2.6.18.dfsg.1-22etch2_arm.deb
    Size/MD5 checksum:   200112 db009d7e4d83679ea6bf62352e8cbe27
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-iop32x_2.6.18.dfsg.1-22etch2_arm.deb
    Size/MD5 checksum:  7925520 1488da3529714a8490790357b2a28d34
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-iop32x_2.6.18.dfsg.1-22etch2_arm.deb
    Size/MD5 checksum:   235346 8bac39d6ded885fc5247046c1a0f5c1c
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-s3c2410_2.6.18.dfsg.1-22etch2_arm.deb
    Size/MD5 checksum:   205074 8fa04e311cd6e56aa2749d4146056c77
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-all-arm_2.6.18.dfsg.1-22etch2_arm.deb
    Size/MD5 checksum:    55234 060e1f4e0b26b2fb42fe0be4d8078952
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6_2.6.18.dfsg.1-22etch2_arm.deb
    Size/MD5 checksum:  3411494 cfe4f5b7273940720e2bc1e945631f1c
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-rpc_2.6.18.dfsg.1-22etch2_arm.deb
    Size/MD5 checksum:  4588214 42d8698407235b46507cf2d7b992c09c
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-footbridge_2.6.18.dfsg.1-22etch2_arm.deb
    Size/MD5 checksum:   234198 35c06e74a9f26ab8217ea98df7c01a9b
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-footbridge_2.6.18.dfsg.1-22etch2_arm.deb
    Size/MD5 checksum:  7567632 3e4a444afcf3df3e510e2c5000a4b684
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-s3c2410_2.6.18.dfsg.1-22etch2_arm.deb
    Size/MD5 checksum:  5010844 a3a1c62fbe80192e80bcaac498036e92
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-ixp4xx_2.6.18.dfsg.1-22etch2_arm.deb
    Size/MD5 checksum:  8872222 e1a64be9bdc5246dd1d9ef666eb0a311

hppa architecture (HP PA RISC)

  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-parisc64-smp_2.6.18.dfsg.1-22etch2_hppa.deb
    Size/MD5 checksum: 11805014 e45047cea390fd98d4a55557414b578e
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-all-hppa_2.6.18.dfsg.1-22etch2_hppa.deb
    Size/MD5 checksum:    55184 397ca7761578ce779b6ef9bacc372ffb
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-parisc_2.6.18.dfsg.1-22etch2_hppa.deb
    Size/MD5 checksum: 10551992 1d693cdd06bb4dee92a25cd886817522
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-all_2.6.18.dfsg.1-22etch2_hppa.deb
    Size/MD5 checksum:    55158 15e7aa685aea41e29ac6c76825e99ad6
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-parisc64-smp_2.6.18.dfsg.1-22etch2_hppa.deb
    Size/MD5 checksum:   199066 5aaf2ce6a96449673f8a8f48f36cf841
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-parisc64_2.6.18.dfsg.1-22etch2_hppa.deb
    Size/MD5 checksum: 11400554 53c5eca027dc1015bf6423cbe65536cd
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-parisc-smp_2.6.18.dfsg.1-22etch2_hppa.deb
    Size/MD5 checksum: 10998698 050980b34cbc925f0842c161828aa1f7
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-parisc64_2.6.18.dfsg.1-22etch2_hppa.deb
    Size/MD5 checksum:   197944 1d45f1fe804821cc7b8aa80b0dec4f36
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6_2.6.18.dfsg.1-22etch2_hppa.deb
    Size/MD5 checksum:  3022526 52b6702172ceb2064dde03ea3cacca7a
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-parisc-smp_2.6.18.dfsg.1-22etch2_hppa.deb
    Size/MD5 checksum:   198152 556e2b16e2988976dac562f1baad15a9
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-parisc_2.6.18.dfsg.1-22etch2_hppa.deb
    Size/MD5 checksum:   197130 298d3e069a1f6996685c4a5a8bc1bada

i386 architecture (Intel ia32)

  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-vserver-686_2.6.18.dfsg.1-22etch2_i386.deb
    Size/MD5 checksum: 16377614 74d1938921db193d10a0ff798fe7c005
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-xen-686_2.6.18.dfsg.1-22etch2_i386.deb
    Size/MD5 checksum:   272450 5468ca009d328fc890a6ab771ab20968
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-k7_2.6.18.dfsg.1-22etch2_i386.deb
    Size/MD5 checksum: 16471330 35ea40d0f873d6c8ff819a62239623ac
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-k7_2.6.18.dfsg.1-22etch2_i386.deb
    Size/MD5 checksum:   280452 b055d4305ed580aa9603c3ff5b8c48ef
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-amd64_2.6.18.dfsg.1-22etch2_i386.deb
    Size/MD5 checksum: 16838150 45beacafd1a4902c055c41bd7711586a
  http://security.debian.org/pool/updates/main/u/user-mode-linux/user-mode-linux_2.6.18-1um-2etch.22etch2_i386.deb
    Size/MD5 checksum: 25589346 bf03c496e08b7d40f52f602011c07560
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-686-bigmem_2.6.18.dfsg.1-22etch2_i386.deb
    Size/MD5 checksum: 16406940 0225f98fb843e23225875943b5179aa1
  http://security.debian.org/pool/updates/main/l/linux-2.6/xen-linux-system-2.6.18-6-xen-686_2.6.18.dfsg.1-22etch2_i386.deb
    Size/MD5 checksum:    55132 28a33a24ebdcdcad85d335499397c88c
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-all-i386_2.6.18.dfsg.1-22etch2_i386.deb
    Size/MD5 checksum:    55196 16b6e16eefaeb5891ab4ba7f832d0ed3
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6_2.6.18.dfsg.1-22etch2_i386.deb
    Size/MD5 checksum:  3169128 eb405a8b5c8b6d815c95f73262a99af1
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-all_2.6.18.dfsg.1-22etch2_i386.deb
    Size/MD5 checksum:    55144 3d96d807e874865af58831720adb4486
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-vserver_2.6.18.dfsg.1-22etch2_i386.deb
    Size/MD5 checksum:  3055432 a496fea546da3a21748b444ffd6836ec
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-xen-vserver-686_2.6.18.dfsg.1-22etch2_i386.deb
    Size/MD5 checksum:   272846 c58bde7235a1f1ac5c8145dc1ae9346b
  http://security.debian.org/pool/updates/main/l/linux-2.6/xen-linux-system-2.6.18-6-xen-vserver-686_2.6.18.dfsg.1-22etch2_i386.deb
    Size/MD5 checksum:    55140 eee1bdfac4bbecc919d0151b2e23d24c
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-modules-2.6.18-6-xen-vserver-686_2.6.18.dfsg.1-22etch2_i386.deb
    Size/MD5 checksum: 14292444 19fe5544166f396830fef3a1b4e4626f
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-686_2.6.18.dfsg.1-22etch2_i386.deb
    Size/MD5 checksum:   280608 496144bbb1d620644189d012b9d6ea70
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-486_2.6.18.dfsg.1-22etch2_i386.deb
    Size/MD5 checksum:   282902 f4145bd74ab9467b1af36da46b7666ef
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-vserver-k7_2.6.18.dfsg.1-22etch2_i386.deb
    Size/MD5 checksum:   279034 26430131dc00922da7c9b034c191eaf1
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-amd64_2.6.18.dfsg.1-22etch2_i386.deb
    Size/MD5 checksum:   272230 871474e9b227166f527391aa064ec971
  http://security.debian.org/pool/updates/main/f/fai-kernels/fai-kernels_1.17+etch.22etch2_i386.deb
    Size/MD5 checksum:  5505296 9da61c3c82edf996a6324ebd4517251c
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-xen_2.6.18.dfsg.1-22etch2_i386.deb
    Size/MD5 checksum:  3149672 c6cbb32f47b75db7d63a6fcf84e0df38
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-xen-vserver_2.6.18.dfsg.1-22etch2_i386.deb
    Size/MD5 checksum:  3171866 333ceba6af38b86e41a1db10fc764319
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-686-bigmem_2.6.18.dfsg.1-22etch2_i386.deb
    Size/MD5 checksum:   281062 c0c403aedd560ad15fb7d90c54ce3145
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-486_2.6.18.dfsg.1-22etch2_i386.deb
    Size/MD5 checksum: 16192240 a4a3d514e989d64042c46d3ad8d0b0ae
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-vserver-686_2.6.18.dfsg.1-22etch2_i386.deb
    Size/MD5 checksum:   278950 9de592d811e51eeb07ebe193f8734d15
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-686_2.6.18.dfsg.1-22etch2_i386.deb
    Size/MD5 checksum: 16337664 a367c6561e7cf8ff701a731d8072dca2
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-xen-vserver-686_2.6.18.dfsg.1-22etch2_i386.deb
    Size/MD5 checksum:  1328006 c5089d6b0fa44ce46fdbbd17140cb9c6
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-vserver-k7_2.6.18.dfsg.1-22etch2_i386.deb
    Size/MD5 checksum: 16509906 c8894d32a5f39e7512d2db9de74ddf3c
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-xen-686_2.6.18.dfsg.1-22etch2_i386.deb
    Size/MD5 checksum:  1300920 9c42c8356ab3a07b89f2d95f47de5303
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-modules-2.6.18-6-xen-686_2.6.18.dfsg.1-22etch2_i386.deb
    Size/MD5 checksum: 14281014 32f281f475d4ee1f2d67dd029231ea46

ia64 architecture (Intel ia64)

  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-mckinley_2.6.18.dfsg.1-22etch2_ia64.deb
    Size/MD5 checksum:   256640 e96b0df79e67e6049d8feeb73e63f287
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-all-ia64_2.6.18.dfsg.1-22etch2_ia64.deb
    Size/MD5 checksum:    55174 c93847739b57f47faacaf642de4ab6c1
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-itanium_2.6.18.dfsg.1-22etch2_ia64.deb
    Size/MD5 checksum:   256732 af4ee0bbafe50ce27d875243fe626ab9
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-mckinley_2.6.18.dfsg.1-22etch2_ia64.deb
    Size/MD5 checksum: 28181368 dff959a14317741919a7a46d73c273a8
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-all_2.6.18.dfsg.1-22etch2_ia64.deb
    Size/MD5 checksum:    55146 9f30373190856528f752d21d66a88053
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-itanium_2.6.18.dfsg.1-22etch2_ia64.deb
    Size/MD5 checksum: 28014200 d290bc90985342dfe3b348d3e1d07922
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6_2.6.18.dfsg.1-22etch2_ia64.deb
    Size/MD5 checksum:  3083012 cbb353d4e8cb3a2bfc4dbf43f4427344

mips architecture (MIPS (Big Endian))

  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-r4k-ip22_2.6.18.dfsg.1-22etch2_mips.deb
    Size/MD5 checksum:  8311782 6b24a125c1b553bd4f69d830f2d19eaa
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-qemu_2.6.18.dfsg.1-22etch2_mips.deb
    Size/MD5 checksum:  6124616 ab5d97cb43f8e7d0f7cfebe2db2416df
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-all_2.6.18.dfsg.1-22etch2_mips.deb
    Size/MD5 checksum:    55190 1cd7be3bb06e63ecf67e6c5c175aa201
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-sb1a-bcm91480b_2.6.18.dfsg.1-22etch2_mips.deb
    Size/MD5 checksum:   187898 a74c79e4b6de9312c1a531b7d1c04e06
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-r4k-ip22_2.6.18.dfsg.1-22etch2_mips.deb
    Size/MD5 checksum:   164036 58db324ddfe7d1b3f7086c67091c1ae3
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-sb1a-bcm91480b_2.6.18.dfsg.1-22etch2_mips.deb
    Size/MD5 checksum: 15654946 4c99171f6b6e8e0b06d88f65244f5a5c
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-qemu_2.6.18.dfsg.1-22etch2_mips.deb
    Size/MD5 checksum:   155578 71edf3d7d29b414ee8fe9c26d98afe2f
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-sb1-bcm91250a_2.6.18.dfsg.1-22etch2_mips.deb
    Size/MD5 checksum: 15680482 79ca37454bab6d11c4af5621e3963e87
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-r5k-ip32_2.6.18.dfsg.1-22etch2_mips.deb
    Size/MD5 checksum:   168240 0eafb18463f917510d3110c74786742d
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6_2.6.18.dfsg.1-22etch2_mips.deb
    Size/MD5 checksum:  3415314 94c72a62a2c3c52535c3ef4936400ad9
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-r5k-ip32_2.6.18.dfsg.1-22etch2_mips.deb
    Size/MD5 checksum:  9077040 581d6744c0c10ec63b06b5d058e44a87
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-all-mips_2.6.18.dfsg.1-22etch2_mips.deb
    Size/MD5 checksum:    55236 7c94ce0a6b000ba9abcc0bf8272e033c
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-sb1-bcm91250a_2.6.18.dfsg.1-22etch2_mips.deb
    Size/MD5 checksum:   188208 2356f7b9cf66b0694b1b7bb07e9e1df0

mipsel architecture (MIPS (Little Endian))

  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6_2.6.18.dfsg.1-22etch2_mipsel.deb
    Size/MD5 checksum:  3351164 f15965863479e2b0eab136c912b25ebd
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-sb1a-bcm91480b_2.6.18.dfsg.1-22etch2_mipsel.deb
    Size/MD5 checksum:   183690 e20c71f57188bc8458abb4dc212402f3
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-r4k-kn04_2.6.18.dfsg.1-22etch2_mipsel.deb
    Size/MD5 checksum:   156886 65c9aec3009d7ace0550bee15d0bd34c
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-qemu_2.6.18.dfsg.1-22etch2_mipsel.deb
    Size/MD5 checksum:   151270 92419bb4255d3989eef1b3d2e616e5a5
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-r5k-cobalt_2.6.18.dfsg.1-22etch2_mipsel.deb
    Size/MD5 checksum:  9864388 2aa86c68a6c8ac36e8dc22aafcac34fb
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-sb1-bcm91250a_2.6.18.dfsg.1-22etch2_mipsel.deb
    Size/MD5 checksum:   183862 262a4f1a05c00b6c2752733101d2bb4b
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-qemu_2.6.18.dfsg.1-22etch2_mipsel.deb
    Size/MD5 checksum:  6030536 1199eb8f7b6a9b4ff1a32ebccfcd7074
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-r3k-kn02_2.6.18.dfsg.1-22etch2_mipsel.deb
    Size/MD5 checksum:  5944692 205d02fd233142d8058874e6ead00e94
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-r3k-kn02_2.6.18.dfsg.1-22etch2_mipsel.deb
    Size/MD5 checksum:   156854 1f02416baf0082238418bba97eb8aa4f
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-sb1a-bcm91480b_2.6.18.dfsg.1-22etch2_mipsel.deb
    Size/MD5 checksum: 15028044 00f2ad06dfa6c908036349c35a5a8e39
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-all_2.6.18.dfsg.1-22etch2_mipsel.deb
    Size/MD5 checksum:    55154 6b6f4c17287d5e053d716a742cb738a0
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-r5k-cobalt_2.6.18.dfsg.1-22etch2_mipsel.deb
    Size/MD5 checksum:   179462 e5497abf20a5461319d8f1d7c37d19b3
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-sb1-bcm91250a_2.6.18.dfsg.1-22etch2_mipsel.deb
    Size/MD5 checksum: 15057484 ea8ab2c273fbc3d282d0120cdbbe5c34
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-all-mipsel_2.6.18.dfsg.1-22etch2_mipsel.deb
    Size/MD5 checksum:    55208 f3bfa65fa08c4ad552aef1536bdd1fcc
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-r4k-kn04_2.6.18.dfsg.1-22etch2_mipsel.deb
    Size/MD5 checksum:  5928064 cad7b6b3653d205a39e876a508d4ddaf

powerpc architecture (PowerPC)

  http://security.debian.org/pool/updates/main/f/fai-kernels/fai-kernels_1.17+etch.22etch2_powerpc.deb
    Size/MD5 checksum:  3367890 ad1ad713d488f9e0e0669e1293f1ccd3
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-powerpc64_2.6.18.dfsg.1-22etch2_powerpc.deb
    Size/MD5 checksum:   254026 ae831a11ed61a4b52bb7a3043eb2f265
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-powerpc_2.6.18.dfsg.1-22etch2_powerpc.deb
    Size/MD5 checksum: 16628466 1b10ecc3a4095b9f0e9cd6fa0c544308
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-vserver_2.6.18.dfsg.1-22etch2_powerpc.deb
    Size/MD5 checksum:  3415704 ea230f1f09a4e8341e7bb24ebd037dd3
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-powerpc-miboot_2.6.18.dfsg.1-22etch2_powerpc.deb
    Size/MD5 checksum: 15160062 e41656dd2782d44ec9487138dc694722
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-vserver-powerpc_2.6.18.dfsg.1-22etch2_powerpc.deb
    Size/MD5 checksum: 17017000 04153c3611b94ae5044868407f1eaa96
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-vserver-powerpc64_2.6.18.dfsg.1-22etch2_powerpc.deb
    Size/MD5 checksum:   255324 60960ac1262f6d8eabcd96d9789b27bc
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-powerpc-smp_2.6.18.dfsg.1-22etch2_powerpc.deb
    Size/MD5 checksum:   253678 e1a1fcc0865698fca0aa46cc02c0e38d
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-powerpc64_2.6.18.dfsg.1-22etch2_powerpc.deb
    Size/MD5 checksum: 18295838 01cb70246fc9027dc9097964d0a7c06d
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-vserver-powerpc_2.6.18.dfsg.1-22etch2_powerpc.deb
    Size/MD5 checksum:   253858 a30720b97122274208888b564c0548b5
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-vserver-powerpc64_2.6.18.dfsg.1-22etch2_powerpc.deb
    Size/MD5 checksum: 18354880 c82075e19802e915551cd25b04f72c76
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6_2.6.18.dfsg.1-22etch2_powerpc.deb
    Size/MD5 checksum:  3393390 246ebdc0ce319ab1b8c0022b1b69fd1d
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-prep_2.6.18.dfsg.1-22etch2_powerpc.deb
    Size/MD5 checksum:   246546 855a95e124dcd81d26d06fced8ff9cda
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-prep_2.6.18.dfsg.1-22etch2_powerpc.deb
    Size/MD5 checksum: 16406142 59c2543679159905e0d16445d7f5b66d
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-powerpc-miboot_2.6.18.dfsg.1-22etch2_powerpc.deb
    Size/MD5 checksum:   231148 96bb009cbc3c1c9fc00a4f84a4aa9cd5
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-all_2.6.18.dfsg.1-22etch2_powerpc.deb
    Size/MD5 checksum:    55156 94c052eb19ce469aabc0a9a71860f861
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-powerpc-smp_2.6.18.dfsg.1-22etch2_powerpc.deb
    Size/MD5 checksum: 16971942 c230eb342210e9f9376783e013216695
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-powerpc_2.6.18.dfsg.1-22etch2_powerpc.deb
    Size/MD5 checksum:   252866 4b2bf2f6783bb456b5c003df3e1ea1f5
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-all-powerpc_2.6.18.dfsg.1-22etch2_powerpc.deb
    Size/MD5 checksum:    55206 209887bf65d555135cde2cb7b1ef0b7b

s390 architecture (IBM S/390)

  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-all-s390_2.6.18.dfsg.1-22etch2_s390.deb
    Size/MD5 checksum:    55168 ba80b3fe0aa1ffdc92825578c4a0a8e2
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-s390x_2.6.18.dfsg.1-22etch2_s390.deb
    Size/MD5 checksum:  5620988 555c4bd0a4b245f267dc324c9b39d695
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-s390_2.6.18.dfsg.1-22etch2_s390.deb
    Size/MD5 checksum:   144604 bd539fa6955b9ea80a38046b1097ea6e
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-vserver-s390x_2.6.18.dfsg.1-22etch2_s390.deb
    Size/MD5 checksum:   145888 80f3dcbc19b2076e6a37ac8f48f1c687
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-s390_2.6.18.dfsg.1-22etch2_s390.deb
    Size/MD5 checksum:  5403802 9bfc6e49de7faecc3ae013efd8a5ff85
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6_2.6.18.dfsg.1-22etch2_s390.deb
    Size/MD5 checksum:  2944158 7a37040db97a3a43817eea570ca29554
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-vserver-s390x_2.6.18.dfsg.1-22etch2_s390.deb
    Size/MD5 checksum:  5663004 f3b7384004cd0c0c6a0478865300b9f8
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-all_2.6.18.dfsg.1-22etch2_s390.deb
    Size/MD5 checksum:    55146 fd4df49a7d2116c1b1eb0f38211c196c
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-s390x_2.6.18.dfsg.1-22etch2_s390.deb
    Size/MD5 checksum:   145096 7e0bfcc155246d4b7cb8ee00d9a370ba
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-s390-tape_2.6.18.dfsg.1-22etch2_s390.deb
    Size/MD5 checksum:  1440072 437e3a46bda63f1ba5bb2d0b970a669f
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-vserver_2.6.18.dfsg.1-22etch2_s390.deb
    Size/MD5 checksum:  2967228 a32e5147c0f741a5300c6122aade4377

sparc architecture (Sun SPARC/UltraSPARC)

  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-sparc64_2.6.18.dfsg.1-22etch2_sparc.deb
    Size/MD5 checksum: 10394174 30c3164de023288678eda955e7088e89
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-sparc32_2.6.18.dfsg.1-22etch2_sparc.deb
    Size/MD5 checksum:   166138 da9c686e82839dbecfc4d2f8d5d6ff8f
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-sparc32_2.6.18.dfsg.1-22etch2_sparc.deb
    Size/MD5 checksum:  6412488 738092d091284463d88dcce9bc0d683e
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-vserver-sparc64_2.6.18.dfsg.1-22etch2_sparc.deb
    Size/MD5 checksum: 10694332 aa5d657822ad30eb7edf2eca35644e09
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-sparc64-smp_2.6.18.dfsg.1-22etch2_sparc.deb
    Size/MD5 checksum:   196630 b349a6dd56888b772577075f21229498
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-sparc64_2.6.18.dfsg.1-22etch2_sparc.deb
    Size/MD5 checksum:   195534 c7b2e3ee30ba33f0a207fa11a5f08916
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-vserver-sparc64_2.6.18.dfsg.1-22etch2_sparc.deb
    Size/MD5 checksum:   197330 5e9f1ad14b5b10c6012b09dcfa93acb5
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-sparc64-smp_2.6.18.dfsg.1-22etch2_sparc.deb
    Size/MD5 checksum: 10647804 5bab66c5cdaa22683425b4e101fea4e0
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-all_2.6.18.dfsg.1-22etch2_sparc.deb
    Size/MD5 checksum:    55156 da09f09d5c47ce3cd8666b7ba98b9a08
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-vserver_2.6.18.dfsg.1-22etch2_sparc.deb
    Size/MD5 checksum:  3191596 f6aee8879affc350feb8706cdffb95da
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6_2.6.18.dfsg.1-22etch2_sparc.deb
    Size/MD5 checksum:  3169298 a6ad7bd3b6ba9d573f47c21f45b9f660
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-all-sparc_2.6.18.dfsg.1-22etch2_sparc.deb
    Size/MD5 checksum:    55184 322a273cbddb53db9fdb143229104384

  These changes will probably be included in the stable distribution on
  its next update.

---------------------------------------------------------------------------------
For apt-get: deb http://security.debian.org/ stable/updates main
For dpkg-ftp: ftp://security.debian.org/debian-security dists/stable/updates/main
Mailing list: debian-security-announce@lists.debian.org
Package info: `apt-cache show <pkg>' and http://packages.debian.org/<pkg>
