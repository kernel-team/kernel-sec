Subject: New Linux kernel 2.6.8 packages fix several issues

--------------------------------------------------------------------------
Debian Security Advisory DSA XXX-1                     security@debian.org
http://www.debian.org/security/                   Dann Frazier, Troy Heber
XXXXX 8th, 2005                         http://www.debian.org/security/faq
--------------------------------------------------------------------------

Package        : kernel-source-2.6.8
Vulnerability  : several
Problem-Type   : local/remote
Debian-specific: no
CVE ID         : CVE-2005-3359 CVE-2006-0038 CVE-2006-0039 CVE-2006-0456
                 CVE-2006-0554 CVE-2006-0555 CVE-2006-0557 CVE-2006-0558
                 CVE-2006-0741 CVE-2006-0742 CVE-2006-0744 CVE-2006-1056
                 CVE-2006-1242 CVE-2006-1368 CVE-2006-1523 CVE-2006-1524
                 CVE-2006-1525 CVE-2006-1857 CVE-2006-1858 CVE-2006-1863
                 CVE-2006-1864 CVE-2006-2271 CVE-2006-2272 CVE-2006-2274
Debian Bug     : 

Several local and remote vulnerabilities have been discovered in the Linux
kernel that may lead to a denial of service or the execution of arbitrary
code. The Common Vulnerabilities and Exposures project identifies the
following problems:

CVE-2005-3359

    Franz Filz discovered that some socket calls permit causing inconsistent
    reference counts on loadable modules, which allows local users to cause
    a denial of service.
    
CVE-2006-0038

    "Solar Designer" discovered that arithmetic computations in netfilter's
    do_replace() function can lead to a buffer overflow and the execution of
    arbitrary code. However, the operation requires CAP_NET_ADMIN privileges,
    which is only an issue in virtualization systems or fine grained access
    control systems.

CVE-2006-0039

    "Solar Designer" discovered a race condition in netfilter's
    do_add_counters() function, which allows information disclosure of kernel
    memory by exploiting a race condition. Likewise, it requires CAP_NET_ADMIN
    privileges. 

CVE-2006-0456

    David Howells discovered that the s390 assembly version of the
    strnlen_user() function incorrectly returns some string size values.

CVE-2006-0554

    It was discovered that the ftruncate() function of XFS can expose
    unallocated, which allows information disclosure of previously deleted
    files.

CVE-2006-0555

    It was discovered that some NFS file operations on handles mounted with
    O_DIRECT can force the kernel into a crash.

CVE-2006-0557

    It was discovered that the code to configure memory policies allows
    tricking the kernel into a crash, thus allowing denial of service.

CVE-2006-0558

    It was discovered by Cliff Wickman that perfmon for the IA64
    architecture allows users to trigger a BUG() assert, which allows
    denial of service.

CVE-2006-0741

    Intel EM64T systems were discovered to be susceptible to a local
    DoS due to an endless recursive fault related to a bad elf entry
    address.

CVE-2006-0742

    Alan and Gareth discovered that the ia64 platform had an
    incorrectly declared die_if_kernel() function as "does never
    return" which could be exploited by a local attacker resulting in
    a kernel crash.

CVE-2006-0744

    The Linux kernel did not properly handle uncanonical return
    addresses on Intel EM64T CPUs, reporting exceptions in the SYSRET
    instead of the next instruction, causing the kernel exception
    handler to run on the user stack with the wrong GS. This may result
    in a DoS due to a local user changing the frames.

CVE-2006-1056

    AMD64 machines (and other 7th and 8th generation AuthenticAMD
    processors) were found to be vulnerable to sensitive information
    leakage, due to how they handle saving and restoring the FOP, FIP,
    and FDP x87 registers in FXSAVE/FXRSTOR when an exception is
    pending. This allows a process to determine portions of the state
    of floating point instructions of other processes.

CVE-2006-1242

    Marco Ivaldi discovered that there was an unintended information
    disclosure allowing remote attackers to bypass protections against
    Idle Scans (nmap -sI) by abusing the ID field of IP packets and
    bypassing the zero IP ID in DF packet countermeasure. This was a
    result of the ip_push_pending_frames function improperly
    incremented the IP ID field when sending a RST after receiving
    unsolicited TCP SYN-ACK packets.

CVE-2006-1368

    Shaun Tancheff discovered a buffer overflow (boundry condition
    error) in the USB Gadget RNDIS implementation allowing remote
    attackers to cause a DoS. While creating a reply message, the
    driver allocated memory for the reply data, but not for the reply
    structure. The kernel fails to properly bounds-check user-supplied
    data before copying it to an insufficiently sized memory
    buffer. Attackers could crash the system, or possibly execute
    arbitrary machine code.

CVE-2006-1523

    Oleg Nesterov reported an unsafe BUG_ON call in signal.c which was
    introduced by RCU signal handling. The BUG_ON code is protected by
    siglock while the code in switch_exit_pids() uses tasklist_lock. It
    may be possible for local users to exploit this to initiate a denial
    of service attack (DoS).

CVE-2006-1524

    Hugh Dickins discovered an issue in the madvise_remove function wherein
    file and mmap restrictions are not followed, allowing local users to
    bypass IPC permissions and replace portions of readonly tmpfs files with
    zeroes.

CVE-2006-1525

    Alexandra Kossovsky reported a NULL pointer dereference condition in
    ip_route_input() that can be triggered by a local user by requesting
    a route for a multicast IP address, resulting in a denial of service
    (panic).

CVE-2006-1857

    Vlad Yasevich reported a data validation issue in the SCTP subsystem
    that may allow a remote user to overflow a buffer using a badly formatted
    HB-ACK chunk, resulting in a denial of service.

CVE-2006-1858

    Vlad Yasevich reported a bug in the bounds checking code in the SCTP
    subsystem that may allow a remote attacker to trigger a denial of service
    attack when rounded parameter lengths are used to calculate parameter
    lengths instead of the actual values.

CVE-2006-1863

    Mark Mosely discovered that chroots residing on an CIFS share can be
    escaped with specially crafted "cd" sequences.

CVE-2006-1864

    Mark Mosely discovered that chroots residing on an SMB share can be
    escaped with specially crafted "cd" sequences.

CVE-2006-2271

    The "Mu security team" discovered that carefully crafted ECNE chunks can
    cause a kernel crash by accessing incorrect state stable entries in the
    SCTP networking subsystem, which allows denial of service.

CVE-2006-2272

    The "Mu security team" discovered that fragmented SCTP control
    chunks can trigger kernel panics, which allows for denial of
    service attacks.

CVE-2006-2274

    It was discovered that SCTP packets with two initial bundled data
    packets can lead to infinite recursion, which allows for denial of
    service attacks.



The following matrix explains which kernel version for which architecture
fix the problems mentioned above:

                                 Debian 3.1 (sarge)
     Source                      2.6.8-16sarge3
     Alpha architecture          2.6.8-16sarge3
     AMD64 architecture          2.6.8-16sarge3
     HP Precision architecture   2.6.8-6sarge3
     Intel IA-32 architecture    2.6.8-16sarge3
     Intel IA-64 architecture    2.6.8-14sarge3
     Motorola 680x0 architecture 2.6.8-4sarge3
     PowerPC architecture        2.6.8-12sarge3
     IBM S/390 architecture      2.6.8-5sarge3
     Sun Sparc architecture      2.6.8-15sarge3

The following matrix lists additional packages that were rebuilt for
compatibility with or to take advantage of this update:

                                 Debian 3.1 (sarge)
     fai-kernels                 1.9.1sarge2

We recommend that you upgrade your kernel package immediately and reboot
the machine. If you have built a custom kernel from the kernel source
package, you will need to rebuild to take advantage of these fixes.

Upgrade Instructions
--------------------

wget url
        will fetch the file for you
dpkg -i file.deb
        will install the referenced file.

If you are using the apt-get package manager, use the line for
sources.list as given below:

apt-get update
        will update the internal database
apt-get upgrade
        will install corrected packages

You may use an automated update by adding the resources from the
footer to the proper configuration.


Debian GNU/Linux 3.1 alias sarge
--------------------------------


  These files will probably be moved into the stable distribution on
  its next update.

---------------------------------------------------------------------------------
For apt-get: deb http://security.debian.org/ stable/updates main
For dpkg-ftp: ftp://security.debian.org/debian-security dists/stable/updates/main
Mailing list: debian-security-announce@lists.debian.org
Package info: `apt-cache show <pkg>' and http://packages.debian.org/<pkg>
