Package: linux
CVE ID: CVE-2021-33655 CVE-2022-2318 CVE-2022-26365 CVE-2022-33740 CVE-2022-33741 CVE-2022-33742 CVE-2022-33743 CVE-2022-33744 CVE-2022-34918

Several vulnerabilities have been discovered in the Linux kernel that
may lead to a privilege escalation, denial of service or information
leaks.

CVE-2021-33655

    A user with access to a framebuffer console driver could cause a memory out-of-bounds write via the
    FBIOPUT_VSCREENINFO ioctl

CVE-2022-2318

    A use-after-free in the Amateur Radio X.25 PLP (Rose) support may result in denial of service.

CVE-2022-26365 / CVE-2022-33740 / CVE-2022-33741 / CVE-2022-33742

    Roger Pau Monne discovered that Xen block and network PV device frontends don't zero out
    memory regions before sharing them with the backend, which may result in information disclosure.
    Additionally it was discovered that the granularity of the grant table doesn't permit sharing
    less than a 4k page, which may also result in information disclosure.

CVE-2022-33743

    Jan Beulich discovered that incorrect memory handling in the Xen network backend may lead
    to denial of service.

CVE-2022-33744

    Oleksandr Tyshchenko discovered ARM Xen guests can cause a denial of service to the Dom0
    via paravirtual devices.

CVE-2022-34918

    Arthur Mongodin discovered a heap buffer overflow in the Netfilter subsystem which may result
    in local privilege escalation.
